/**
 *  A directive which helps you easily show a baidu-map on your page.
 *
 *
 *  Usages:
 *
 *      <baidu-map options="options"></baidu-map>
 *
 *      options: The configurations for the map
 *            .width[Number]{M}:        The width of the map
 *            .height[Number]{M}:       The height of the map
 *            .center.longitude[Number]{M}: The longitude of the center point
 *            .center.latitude[Number]{M}: The latitude of the center point
 *            .zoom[Number]{O}:         Map's zoom level. This must be a number between 3 and 19
 *            .navCtrl[Boolean]{O}:     Whether to add a NavigationControl to the map
 *            .scaleCtrl[Boolean]{O}:   Whether to add a ScaleControl to the map
 *            .overviewCtrl[Boolean]{O}: Whether to add a OverviewMapControl to the map
 *            .enableScrollWheelZoom[Boolean]{O}: Whether to enableScrollWheelZoom to the map
 *            .city[String]{M}:         The city name which you want to display on the map
 *            .markers[Array]{O}:       An array of marker which will be added on the map
 *                   .longitude{M}:                The longitude of the marker
 *                   .latitude{M}:                 The latitude of the marker
 *                   .icon[String]{O}:             The icon's url for the marker
 *                   .width[Number]{O}:            The icon's width for the icon
 *                   .height[Number]{O}:           The icon's height for the icon
 *                   .title[String]{O}:            The title on the infowindow displayed once you click the marker
 *                   .content[String]{O}:          The content on the infowindow displayed once you click the marker
 *                   .enableMessage[Boolean]{O}:   Whether to enable the SMS feature for this marker window. This option only available when title/content are defined.
 *
 *
 *
 *  @author      Howard.Zuo
 *  @copyright   April 9, 2014
 *  @version     1.0.4
 *
 */
(function(angular) {
    "use strict";

    var defaults = {
        navCtrl: true,
        scaleCtrl: true,
        overviewCtrl: true,
        enableScrollWheelZoom: true,
        zoom: 10,
        driverRoute: false
    };

    var checkNull = function(obj) {
        return obj === null || obj === undefined;
    };

    var checkMandatory = function(prop, desc) {
        if (!prop) {
            throw new Error(desc);
        }
    };

    /**
     * Construction function
     *
     * @constructor
     */
    var baiduMapDir = function() {

        // Return configured, directive instance
        var drawInfo = function(opts) {
            if (!opts || !opts.map) {
                return;
            }
            var map = opts.map,
                h = opts.height || 20,
                w = opts.width || 100,
                offset = opts.offset || {
                    top: 30,
                    left: 0
                };

            // 定义自定义覆盖物的构造函数  
            var SquareOverlay = function(center, w) {
                this._center = center;
                this._width = w;
                this._div;
            }
            // 继承API的BMap.Overlay    
            SquareOverlay.prototype = new BMap.Overlay();
            // 实现初始化方法  
            SquareOverlay.prototype.initialize = function(map) {
                // 保存map对象实例   
                this._map = map;
                // 创建div元素，作为自定义覆盖物的容器   
                var div = document.createElement("div");
                //                          div.className="pop";
                div.style.position = "absolute";
                // 可以根据参数设置元素外观   
                div.style.width = this._width + "px";
                div.style.textAlign = 'center';
                div.style.minHeight = this._height + "px";
                //                          div.style.background = this._color;
                // 将div添加到覆盖物容器中   
                map.getPanes().markerPane.appendChild(div);
                // 保存div实例   
                this._div = div;
                // 需要将div元素作为方法的返回值，当调用该覆盖物的show、   
                // hide方法，或者对覆盖物进行移除时，API都将操作此元素。   
                return div;
            }
            SquareOverlay.prototype.setCenter = function(position) {
                // this._center = center;
                 this._div.style.left = position.x - this._width / 2 - offset.left + "px";
                this._div.style.top = position.y - offset.top + "px";

            }
            //给信息弹框设置样式
            SquareOverlay.prototype.set_Css = function(css) {
                for (var name in css) {
                    this._div.style[name] = css[name];
                }
            }
            //  给信息弹框设置innerHTML
            SquareOverlay.prototype.setContent = function(content) {
                this._div.innerHTML = content;
                //this._div.innerHTML = '<div class="pop">上海市青浦城区营业部>>  </div>';
            }
            // 实现绘制方法   
            SquareOverlay.prototype.draw = function() {
                // 根据地理坐标转换为像素坐标，并设置给容器    
                var position = this._map.pointToOverlayPixel(this._center);
                this._div.style.left = position.x - this._width / 2 - offset.left + "px";
                this._div.style.top = position.y - offset.top + "px";
                //this._div.innerHTML = '<div class="pop">上海市青浦城区营业部>>  </div>';
            }
            // 实现显示方法    
            SquareOverlay.prototype.show = function() {
                if (this._div) {
                    this._div.style.display = "";
                }
            }
            // 实现隐藏方法  
            SquareOverlay.prototype.hide = function() {
                if (this._div) {
                    this._div.style.display = "none";
                }
            }
            SquareOverlay.prototype.toggle = function() {
                if (this._div) {
                    if (this._div.style.display == "") {
                        this.hide();
                    } else {
                        this.show();
                    }
                }
            }

            var mySquare = new SquareOverlay(map.getCenter(), w);
            return mySquare;

        }
        return {
            restrict: 'E',
            scope: {
                'options': '='
            },
            link: function($scope, element, attrs) {
                // attrs.$observe("options",function(){
                    
                // });
                // $scope.$watch($scope.$parent.$parent.mapOptions.markers,function(){
                //     alert("属性值变了");
                // })
                var ops = {}, hasCityName = false,infoWind;
                ops.navCtrl = checkNull($scope.options.navCtrl) ? defaults.navCtrl : $scope.options.navCtrl;
                ops.scaleCtrl = checkNull($scope.options.scaleCtrl) ? defaults.scaleCtrl : $scope.options.scaleCtrl;
                ops.overviewCtrl = checkNull($scope.options.overviewCtrl) ? defaults.overviewCtrl : $scope.options.overviewCtrl;
                ops.enableScrollWheelZoom = checkNull($scope.options.enableScrollWheelZoom) ? defaults.enableScrollWheelZoom : $scope.options.enableScrollWheelZoom;
                ops.zoom = checkNull($scope.options.zoom) ? defaults.zoom : $scope.options.zoom;
                ops.driverRoute = checkNull($scope.options.driverRoute) ? defaults.driverRoute : $scope.options.driverRoute;
                checkMandatory($scope.options.width, 'options.width must be set');
                checkMandatory($scope.options.height, 'options.height must be set');
                checkMandatory($scope.options.center, 'options.center must be set');
                if (typeof $scope.options.center == "string") {
                    hasCityName = true;
                } else {
                    checkMandatory($scope.options.center.longitude, 'options.center.longitude must be set');
                    checkMandatory($scope.options.center.latitude, 'options.center.latitude must be set');

                }
                checkMandatory($scope.options.city, 'options.city must be set');

                ops.width = $scope.options.width;
                ops.height = $scope.options.height;
                if (!hasCityName) {
                    ops.center = {
                        longitude: $scope.options.center.longitude,
                        latitude: $scope.options.center.latitude
                    };
                }
                ops.city = $scope.options.city;
                ops.markers = $scope.options.markers;

                element.find('div').css('width', ops.width);
                element.find('div').css('height', ops.height);

                // create map instance
                var map = new BMap.Map(element.find('div')[0]);

                // init map, set central location and zoom level
                if (!hasCityName) {
                    map.centerAndZoom(new BMap.Point(ops.center.longitude, ops.center.latitude), ops.zoom);
                } else {
                    map.centerAndZoom($scope.options.center);
                }
                if (ops.driverRoute) {
                    var p1 = new BMap.Point($scope.options.ori_local[0], $scope.options.ori_local[1]);
                    var p2 = new BMap.Point($scope.options.dest_local[0], $scope.options.dest_local[1]);
                    var driving = new BMap.DrivingRoute(map, {
                        renderOptions: {
                            map: map,
                            autoViewport: true
                        }
                    });
                    driving.search(p1, p2);
                }
                if (ops.navCtrl) {
                    // add navigation control
                    map.addControl(new BMap.NavigationControl());
                }
                if (ops.scaleCtrl) {
                    // add scale control
                    map.addControl(new BMap.ScaleControl());
                }
                if (ops.overviewCtrl) {
                    //add overview map control
                    map.addControl(new BMap.OverviewMapControl());
                }
                if (ops.enableScrollWheelZoom) {
                    //enable scroll wheel zoom
                    map.enableScrollWheelZoom();
                }

                // set the city name
                map.setCurrentCity(ops.city);

                if (!ops.markers) {
                    return;
                }
                //create markers
                map.addEventListener("click",function(){
                    infoWind&&infoWind.hide();
                });
                var openInfoWindow = function(marker) {
                    return function(e) {
                        // console.log("点击事件" + e);
                        e.domEvent.stopPropagation();
                        var opts={map:map,width:320}
                        if(!infoWind){
                            infoWind=drawInfo(opts);
                            map.addOverlay(infoWind);
                        }
                        map.setCenter(new BMap.Point(marker.longitude, marker.latitude));
                       var position = map.pointToOverlayPixel(new BMap.Point(marker.longitude, marker.latitude));
                       infoWind.addEventListener("click",function(e){
                         e.domEvent.stopPropagation();
                       });
                        infoWind.setCenter(position);
                        infoWind.hide();
                        infoWind.setContent('<div class="pop">'+marker.name+'>></div>');
                        infoWind.show();
                        document.querySelector(".scroll").style.webkitTransform = 'translate3d(0px, -215px, 0px) scale(1)';
                        // this.openInfoWindow(infoWin);
                        $scope.$parent.$parent.testClick(marker.longitude, marker.latitude, marker.name, marker.location_address, marker.business, marker.telephone, marker.distance);
                    };
                };

                for (var i in ops.markers) {
                    var marker = ops.markers[i];
                    var pt = new BMap.Point(marker.longitude, marker.latitude);
                    var marker2;
                    if (marker.icon) {
                        var icon = new BMap.Icon(marker.icon, new BMap.Size(marker.width, marker.height));
                        marker2 = new BMap.Marker(pt, {
                            icon: icon
                        });
                    } else {
                        marker2 = new BMap.Marker(pt);
                    }
                    // add marker to the map
                    map.addOverlay(marker2); // 将标注添加到地图中

                    if (!marker.title && !marker.content) {
                        return;
                    }
                    var infoWindow2 = new BMap.InfoWindow("<p>" + (marker.title ? marker.title : '') + "</p><p>" + (marker.content ? marker.content : '') + "</p>", {
                        enableMessage: checkNull(marker.enableMessage) ? false : marker.enableMessage
                    });
                    marker2.addEventListener("click", openInfoWindow(marker));
                }
            },
            template: '<div id="mapContainer"></div>'
        };
    };

    var baiduMap = angular.module('baiduMap', []);
    baiduMap.directive('baiduMap', [baiduMapDir]);

})(angular);