angular.module('starter.findBackPasswordControllers', []) 
    //找回密码页面
     .controller('findBackPasswordCtrl', function ($scope,$timeout,$http,$location,$rootScope,$ionicLoading,toastService) {
    	 $scope.errorText="";
         $scope.checkPhone= function(){
             if($scope.phone=="" |$scope.phone===undefined |!$scope.findBackPassword.phone.$valid){
                 $scope.errorText="提示：请输入正确的手机号！";
             }else{
            	 $http.post('../login/validatePhone.jspa',{"phone":$scope.phone}).success(function (json) {
                     var detail = eval("(" + json.detail + ")");
                     if(detail.code!="20001"){
                         $scope.errorText="提示：请输入已注册的手机号！";
                     }else{
                         $scope.errorText="";
                     }
                 })
             }
         }
         $scope.checkCd= function(){
             if($scope.validateCode==""|$scope.validateCode===undefined| !$scope.findBackPassword.validateCode.$valid){
                 $scope.errorText="提示：验证码不能为空！";
             }else{
                 $scope.errorText="";
             }
         }
         $scope.checkPassword= function(){
             if($scope.password==""|$scope.password===undefined|!$scope.findBackPassword.password.$valid){
                 $scope.errorText="提示：密码必须为6到16位，请勿包含字母和数字以外的字符！";
             }else{
                 $scope.errorText="";
             }
         }
         $scope.checkNewPwd= function(){
             if($scope.password==""|$scope.password===undefined|!$scope.findBackPassword.newPwd.$valid){
                 $scope.errorText="提示：密码必须为6到16位，请勿包含字母和数字以外的字符！";
             }else if($scope.password!=$scope.newPwd){
                 $scope.errorText="提示：密码不一致，请核实！";
             }
             else{
                 $scope.errorText="";
             }
         }
    	 
        $scope.checkmm = function () {
            
            if($scope.phone==null) {
                $scope.errorText="提示：信息不能为空!";
                $timeout(function () {
            		$scope.errorText="";
                }, 5000);
            }else{
                if ($scope.findBackPassword.$valid) {
	            var obj={"phone":$scope.phone,"validateCode":$scope.validateCode,"newPwd":baseMEncode($scope.newPwd)};
	            $scope.isShow=true;//显示加载框
	            $http.post('../login/findBackMethod.jspa',obj).success(function (json) {
	            	 $scope.isShow=false;//隐藏加载框
	            	var detail = eval("(" + json.detail + ")");
                    if (detail.code == '10000') {
                    	
                    	toastService.load('1',"恭喜您找回密码成功！");//显示提示框
                    	$location.path("/login/myDoppon");
//                    	$ionicLoading.show({
//                            template: '<div style="background:#ffffff;padding:15px;text-align: center;"><img src="img/order_success.png" style="width:35%;margin-bottom: 10px"/><p style="color:#171723;font-size: 15px;font-weight: bold">恭喜您找回密码成功!</p></div>'
//                        });
//                        $timeout(function() {
//                            $ionicLoading.hide();
//                            $location.path("/login/myDoppon");
//                        }, 2000);
                    	
                        
                    } else if(detail.code == '20002') {
                        $scope.errorText = "提示：请输入正确的手机号";
                    }else if(detail.code == '20006'||detail.code == '20007') {
                        $scope.errorText = "提示：短信验证码错误，请核实";
                    }else if(detail.code == '20008'){
                    	$scope.errorText = "提示：您的短信验证码已失效，请重新获取";
                    }else if(detail.code == '20009'){
                    	$scope.errorText = "提示：用户不存在或未绑定";
                    }
                    else{
                    	toastService.load('0',"找回密码失败，请稍后重试！");//显示提示框
                    }
//                        $timeout(function () {
//                            $scope.errorText = "";
//                        }, 5000);
	            	
	            });
           
           }
         }
        }
        $scope.codeText = '获取验证码';
        $scope.timeLength = 60;        
        $scope.checkcode = function (event) {
        	
        	 $http.post('../login/validatePhone.jspa',{"phone":$scope.phone}).success(function (json) {
                var detail = eval("(" + json.detail + ")");
                if(detail==""||detail=="null"||detail===undefined){
                	return
                }else{
                if(detail.code=="20001") {
                	event.target.disabled = false;
        	//调用获取短信编码接口
            if ($scope.phone ===undefined | $scope.phone == null) {
                $scope.errorText="提示：请输入手机号！";
                event.target.disabled = false;
                $scope.codeText = '获取验证码';
                $scope.timeLength = 60;
            } else {
                    $scope.phone= $scope.findBackPassword.phone.$viewValue;
                    var obj={"phone":$scope.phone,"codeType":'2'};
                    $http.post('../login/obtainMessageCode.jspa',obj).success(function(json) {
              			  
              			  });
        	
                    var t = function () {
                        $scope.timeLength--;
                        $scope.codeText = $scope.timeLength + '秒后重新发送';
                        //倒计时结束后
                        if ($scope.timeLength == 0) {
                            $timeout.cancel();
                            $scope.timeLength = 60;
                            event.target.disabled = false;
                            $scope.codeText = '重新获取';
                            return;
                        }
                        $timeout(t,1000);
                    }
                    $timeout(t,1000);
                }
                } else{
                    $scope.errorText="提示：请输入已注册的手机号！";
                }
                }
                })
        };

        $scope.check=function(){  check($scope,$http,$location,$rootScope);}
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
    })
;
