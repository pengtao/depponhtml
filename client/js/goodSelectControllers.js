//货物追踪
angular.module('starter.goodSelectControllers', [])
    //查询页面
	.controller('goodSelectCtrl', function ($scope, $http, $ionicModal,$location,$ionicScrollDelegate,$rootScope,toastService) {
    	
    	/**
         * 保存运单号
         */
    	
        $scope.saveBillNos = function(){
        	
        	//将运单号保存在缓存中
        	$scope.billNo = $scope.$$childHead.billNo;
        	//判断缓存是否为空
         if(localStorage.getItem("billNo") == null ){
        	 //如果空，则创建运单号数组
			billNos = new Array();
			//页面获取的运单号注入数组中
			billNos.push($scope.billNo);
			//运单数组转成字符串形式存在缓存中
			localStorage.setItem("billNo",JSON.stringify(billNos));
			}else{
				
			//将运单号转成json形式
			var noArr = JSON.parse(localStorage.getItem("billNo"));
			//var flag=false;
			for(var i = 0;i < noArr.length;i++)
			{
					if(noArr[i] == $scope.billNo)
					{
						noArr.splice(i,1);
						
						 break;
			     	}
						
			
			}
		
		
	//	if(flag==false){
        noArr.push($scope.billNo);
     	//运单数组转成json形式存在缓存中
		localStorage.setItem("billNo",JSON.stringify(noArr));
		
		//	}
		}
        }
         /**
          * 查询运单号
          */
        	//初始化billNn
        var billNn = {};
        //展示运单号
         $scope.showBills = function(){
        	 //初始化billNo数组
          var billNo = [];
          //从缓存中获取运单号并转换成json类型
          var billArr = JSON.parse(localStorage.getItem("billNo"));
          //判断运单号是否为空
          if(billArr != null){
        	  //如果运单号不为空，且长度0-10
        	  if(billArr.length<10){
        		  //遍历运单号
        		  for(var j = 0,i = billArr.length;i >= 0;i--){
        			  billNn[j] = {
       	        			  billNo:billArr[i]
       	        	  };
        			  //缓存中获取的运单号作为查询单号
        			  $scope.billNn = billNn;
        			  j++;
        		  }
        	  }
        	  //如果运单号数组长度大于等于10
        	  if(billArr.length>=10){
        		  //遍历数组
        		  for(var j = 0, i = billArr.length-1,k = billArr.length-10; i >= k;i--){
        			  //倒叙排列运单号
		        		  billNn[j] = {
			        			  billNo:billArr[i]
			        	  };
		        		  j++;
		        	//缓存中获取的运单号作为查询单号  
	        	  $scope.billNn = billNn;
        		  }
        	  }
          }else{
        	  //如果运单号无 则为空
        	  $scope.billNn = "";
          }
         }
         //初始化
         $scope.showBills();
  

 
         //查询方法
    	$scope.toGoodTrack = function (billNo){
    	
    		
    		if(billNo!=null&&billNo!=""){
    			if(!(/^[1-9][0-9]{6,8}[0-9]$/).test(billNo))
    			{
    				toastService.load('0',"运单号须为8到10位数字！");
    			
    			}else
    				{
    					$http.post('../WayBillNo/getGoodsTrace.jspa',{"keyword":$scope.billNo}).success(function (json) {
                    	$scope.isShow=false;
                    	if(json!=null){
                        	var detail = eval("(" + json.detail + ")");
                        	 if(detail!=null&&detail != ""&&detail.code=='10000'){
                        		//保存运单号到缓存
                   			 $scope.saveBillNos();
                   			 //展示运单号
                 			  $scope.showBills();
                 			//运单号关联到查询结果界面
                 			 $scope.toGoodsTrack(billNo);
                        	 }else{
                         		//运单号关联到查询结果界面
                  			  $scope.toGoodsTrack(billNo);
                      		}
                    	}
                    	
                    });
    				
    			//保存运单号到缓存
    			// $scope.saveBillNos();
  			  //展示运单号
  			 // $scope.showBills();
  			//运单号关联到查询结果界面
  			//  $scope.toGoodsTrack(billNo);
  			  }
    		}
        }
    	//跳转到 货物追踪详情页面
    	$scope.toGoodsTrack = function(billNo){
    		$location.path("/goodsTrack/"+billNo);
    	};
    	$scope.check=function(){  check($scope,$http,$location,$rootScope);}
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}

      $scope.check3=function(){
          $rootScope.goBackNot = true;
          $location.path("/goodSelect");
      }
      $scope.check4=function(){
          $rootScope.goBackNot = true;
          $location.path("/queryBranches");
      }
      $scope.check5=function(){
          $rootScope.goBackNot = true;
          $location.path("/realTimePrice");
      }
      $scope.check6=function(){
          $rootScope.goBackNot = true;
          $location.path("/expressDispatch");
      }
      $scope.goHome=function(){
          $rootScope.goBackNot = true;
          $location.path("#");
      }
    })


