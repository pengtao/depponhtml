angular.module('starter.depponAndPersonControllers', []) 
//我的德邦页面
.controller('myDepponCtrl', function ($scope,$http,$location,$rootScope) {
	$scope.isShow=true;//显示加载框
	$http.post('../login/getSessionInfo.jspa').success(function (json) {
		$scope.isShow=false;//隐藏加载框
		$scope.user =json.userRest;
    	if($scope.user==null){
			$location.path("/login/myDeppon");
		}
    }).error(function () {
        $location.path("/login/myDeppon");
    });
	
    $scope.check=function(){  check($scope,$http,$location,$rootScope);}
    $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}

    $scope.exit = function () {
    	$scope.isShow=true;//显示加载框
    	$http.post('../login/ClearSession.jspa').success(function (json) {
    		$scope.isShow=false;//隐藏加载框
    			$location.path("/login/myDeppon");
       });
    	
    }
})

	 

//个人资料页面
.controller('personInformationCtrl', function ($scope,$http,$location,$timeout,$rootScope,personInfo) {
	$scope.isShow=true;//显示加载框
	$http.post('../login/getSessionInfo.jspa').success(function (json) {
		$scope.isShow=false;//隐藏加载框
    	$scope.user =json.userRest;
    	if($scope.user==null){
			$location.path("/login/myDeppon");
		}else{
			if(personInfo.getPersonInfo()==null){
			    $scope.isShow=true;//显示加载框
		    	$http.post('../user/getPersonDetail.jspa').success(function (json) {
		    		var detail = eval("(" + json.detail + ")");
		    		$scope.isShow=false;//隐藏加载框
		    		if(detail.code=='10000'){
		    			$scope.realName=detail.userRest.realName;
		    			$scope.gender=detail.userRest.gender;
		    			$scope.telephone=detail.userRest.telephone;
		    			var person={
		    					realName:$scope.realName,
		    					gender:$scope.gender,
		    					telephone:$scope.telephone
		    			};
		    			personInfo.setPersonInfo(person);
		    		}else{
				$scope.errorText="提示：数据修复中，请稍后重试!";
			}
		  });
			}else{
				var personDetail=personInfo.getPersonInfo();
				$scope.realName=personDetail.realName;
				$scope.gender=personDetail.gender;
				$scope.telephone=personDetail.telephone;
			}
		}
	 }).error(function () {
	        $location.path("/login/myDeppon");
	    });
	
    $scope.check=function(){  check($scope,$http,$location,$rootScope);}
    $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
    
    
    $scope.submit=function(){
    	if($scope.realName==null|$scope.realName==""|$scope.gender==null|$scope.gender=="") {
            $scope.errorText="提示：信息不能为空!";
            $timeout(function () {
        		$scope.errorText="";
            }, 3000);
        }else{
        	$scope.isShow=true;//显示加载框
    	var obj={"userName":$scope.user.userName,"realName":$scope.realName,"gender":$scope.gender};
        $http.post('../user/saveInfo.jspa',obj).success(function (json) {
        	$scope.isShow=false;//隐藏加载框
        	var detail = eval("(" + json.detail + ")");
            if (detail.code == '10000') {
            	var person={
    					realName:$scope.realName,
    					gender:$scope.gender,
    					telephone:$scope.telephone
    			};
            	personInfo.setPersonInfo(person);
                $location.path("/myDeppon");
            }else if(detail.code == '20009') {
                $scope.errorText = "提示：用户不存在";
            }else if(detail.code == '20012'||detail.code == '20016') {
                $scope.errorText = "提示：用户名为空";
            }else{
                $scope.errorText = "提示：服务器繁忙！";
            }
                $timeout(function () { 
                    $scope.errorText = "";
                }, 3000);
        });
      }
    }
//    $scope.$on('$destroy',function(){
//    	$rootScope.users="";
//    });

});
