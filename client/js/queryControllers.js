angular.module('starter.queryControllers', [])
    /**根据省市区查询快递收发范围信息*/
    .controller('expressDispatchCtrl', function ($scope, $http, $ionicModal,$location,$ionicScrollDelegate,$rootScope) {
    	$rootScope.goBackNot= false;
        $scope.area = {};
        $scope.receive;
        $scope.sender;
        $scope.tshow=true;
        $scope.toggle = function () {
        	if($scope.area.name==null||$scope.area.name == ""){
        		return;
        	}
        	$scope.tshow=false;
           //通过Ajax获取信息
        	 $scope.isShow=true;
            $http.post('../expressRange/queryExpressRange.jspa',{"keyword":$scope.area.code}).success(function (json) {
            	 $scope.isShow=false;
//            	console.log(json);    
            	var detail = eval("(" + json.detail + ")");
            	if(detail!=null&&detail.code=='10000'){
            		$scope.receive =detail.scope.conAreaDesc;
            		$scope.sender =detail.scope.sendAreaDesc;
            	}else{
            		$scope.receive ="抱歉，暂无结果，请谅解 !";
            		$scope.sender ="抱歉，暂无结果，请谅解 !";
            	}

              }).error(function (data, status, headers, config) {
            	    $scope.receive ="抱歉，暂无结果，请谅解 !";
          			$scope.sender ="抱歉，暂无结果，请谅解 !";
              });
        }
        ///////////////////////

        business=4;
        citys($scope, $http,$location,$ionicModal,$ionicScrollDelegate,$rootScope);
        
        $scope.check=function(){  check($scope,$http,$location,$rootScope);}
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
        $scope.check3=function(){
            $rootScope.goBackNot = true;
            $location.path("/goodSelect");
        }
        $scope.check4=function(){
            $rootScope.goBackNot = true;
            $location.path("/queryBranches");
        }
        $scope.check5=function(){
            $rootScope.goBackNot = true;
            $location.path("/realTimePrice");
        }
        $scope.check6=function(){
            $rootScope.goBackNot = true;
            $location.path("/expressDispatch");
        }
        $scope.goHome=function(){
            $rootScope.goBackNot = true;
            $location.path("#");
        }
    })
    /**价格时效查询*/
    .controller('realTimePriceCtrl', function ($scope, $http, $ionicModal,$location,$ionicScrollDelegate,$rootScope,toastService) {
    	$rootScope.goBackNot= false;
    	function isMobile() {
    		var ua = navigator.userAgent;
    		isIOS = /iPhone|iPad|iPod/i.test(ua);
    	    if (isIOS)
    	        isMobile = 'IOS';
    	    return isMobile;
    	}
	     var isMobile=isMobile();
	     $scope.isIOS='tel';
	     if(isMobile=='IOS'){
	    	 $scope.isIOS='number';
	     }else{
	    	 $scope.isIOS='tel';
	     }
    	$scope.sel={};
    	$scope.sel.agent_type2=110;
        $scope.agentTypeModel = [{
            id: 110,
            typeName: '即日退'
        }, {
            id: 100,
            typeName: '三日退'
        }];
        $scope.sel.sign_back=0;
        $scope.signBackModel = [{
            id: 0,
            typeName: '无需返单：0元'
        }, {
            id: 1,
            typeName: '签收单传真返回：10元'
        }, {
            id: 2,
            typeName: '签收单原件返回：20元'
        }];
        $rootScope.controlSystemBackButtonPrice = false;
        $ionicModal.fromTemplateUrl('templates/query/realTimePriceResult.html', {
            scope: $scope,
            animation: 'slide-in-right'
        }).then(function (modal) {
            $scope.priceModal = modal;
        });
        $scope.myVal = true;
        $scope.ionarrow = 'ic_ion-arrow-down-b';
        $scope.prices = [];
        $scope.dishide=false;
        $scope.dishide2=true;
        $scope.errorText="";
        $scope.areaValues;
        $scope.areaCodes;
        $scope.realopenModal = function () {
        	$rootScope.controlSystemBackButtonPrice = true;
            //$scope.sel={"arrivedAreaCode":$scope.sel.arrivedAreaCode,"leavedAreaCode":$scope.sel.leavedAreaCode};
        	if($scope.sel.arrivedAreaCode==null||$scope.sel.arrivedAreaCode==""||$scope.sel.leavedAreaCode==null||$scope.sel.leavedAreaCode==""){
            	toastService.load('0',"请选择收发货城市");
                return;
            }
            /*if(!$scope.myVal){
            	if(($scope.sel.weight==null||$scope.sel.weight=="")||
                        ($scope.sel.volume==null||$scope.sel.volume=="")||
                        ($scope.sel.insured_money==null||$scope.sel.insured_money=="")){
                	alert("请填写重量，体积，保价声明");
                    return;
                }
            }*/
            $scope.sel.agent_type=$scope.sel.agent_type2;
            if($scope.sel.agent_type2==110&&($scope.sel.agent_money==null||$scope.sel.agent_money=="")){
            	$scope.sel.agent_type=0;
            }
            $scope.checkGusuan();
            if($scope.errorText!=""){
            	return;
            }
            $scope.nullArea=false;
            $scope.isShow=true;
        	$http.post('../realtime/queryRealTimePrice.jspa', $scope.sel).success(function (json) {
        		$scope.isShow=false;
                var detail = eval("(" + json.detail + ")");
                if (detail!=null&&detail != "") {//请求成功
                    $scope.prices = detail;
                    if(($scope.sel.weight==null||$scope.sel.weight=="")&&
                        ($scope.sel.volume==null||$scope.sel.volume=="")&&
                        ($scope.sel.insured_money==null||$scope.sel.insured_money=="")&&
                        ($scope.sel.agent_type==null||$scope.sel.agent_type=="")&&
                        ($scope.sel.agent_money==null||$scope.sel.agent_money=="")&&
                        ($scope.sel.sign_back==null||$scope.sel.sign_back=="")){//只根据地址 查询
                    	$scope.dishide=false;
                    	$scope.dishide2=true;
                    }else{
                    	$scope.dishide=true;
                    	$scope.dishide2=false;
                    }
                } else {
                	 $scope.prices=[];
                     $scope.nullArea=true;
                }
                $scope.priceModal.show();
            }).error(function (data, status, headers, config) {
            	$scope.prices=[];
                $scope.nullArea=true;
            }).finally(function () {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });

        };
        $scope.realcloseModal = function () {
        	$rootScope.controlSystemBackButtonPrice = false;
            $scope.priceModal.hide();
        };
        $scope.$on('$destroy', function() {
            $scope.priceModal.remove();
        });
        $scope.$on('$locationChangeStart', function(e) {
            if($rootScope.controlSystemBackButtonPrice){
                $scope.priceModal.hide();
                $rootScope.controlSystemBackButtonPrice = false;
                if($rootScope.goBackNot){
                    $location.path($location.path());
                }else{
                    //拦截系统返回按钮
                    e.preventDefault();

                }
            }
        });
        
        $scope.check=function(){  check($scope,$http,$location,$rootScope);}
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}

      $scope.check3=function(){
          $rootScope.goBackNot = true;
          $location.path("/goodSelect");
      }
      $scope.check4=function(){
          $rootScope.goBackNot = true;
          $location.path("/queryBranches");
      }
      $scope.check5=function(){
          $rootScope.goBackNot = true;
          $location.path("/realTimePrice");
      }
      $scope.check6=function(){
          $rootScope.goBackNot = true;
          $location.path("/expressDispatch");
      }
      $scope.goHome=function(){
          $rootScope.goBackNot = true;
          $location.path("#");
      }
      $scope.checkGusuan = function () {
    	  	if(!$scope.time.weight.$valid){
    	  		$scope.errorText="请正确输入重量";
    	  		toastService.load('0',$scope.errorText);
	      		return;
      		}
    	  	if(!$scope.time.volume.$valid){
    	  		$scope.errorText="请正确输入体积";
    	  		toastService.load('0',$scope.errorText);
        		return;
        	}
    	  	if(!$scope.time.agent_money.$valid){
    	  		$scope.errorText="请正确输入代收货款";
    	  		toastService.load('0',$scope.errorText);
        		return;
        	}
    	  	if(!$scope.time.insured_money.$valid){
    	  		$scope.errorText="请正确输入保价声明";
    	  		toastService.load('0',$scope.errorText);
        		return;
        	}
    	  	$scope.errorText="";
      }
        $scope.checkWeight = function () {
        	if(!$scope.time.weight.$valid){
        		toastService.load('0',"请正确输入重量");
        		return;
        	}else{
        		var weight=$scope.time.weight.$viewValue;
        		if(weight!=""){
        			weight=clearNoNum(weight+"");
                	if(weight.indexOf(".")>=0){
                		weight=weight.substr(0,weight.indexOf(".")+3);
                	}
                	var le=6;
                	if(weight.indexOf(".")<0){
                		if(weight.length>le){
                			weight=weight.substr(0,le);
                		}
                	}else if(weight.indexOf(".")>le){
                		weight=weight.substr(0,le)+weight.substr(weight.indexOf("."),weight.length-1);
                	}
                	$scope.sel.weight=parseFloat(weight);
        		}
        		
        	}
        }
        $scope.checkVolume = function () {
        	if(!$scope.time.volume.$valid){
        		toastService.load('0',"请正确输入体积");
        		return;
        	}else{
        		var volume=$scope.time.volume.$viewValue;
        		if(volume!=""){
        			volume=clearNoNum(volume);
        			if(volume.indexOf(".")>=0){
        				volume=volume.substr(0,volume.indexOf(".")+3);
                	}
    	        	var le=6;
    	        	if(volume.indexOf(".")<0){
    	        		if(volume.length>le){
    	        			volume=volume.substr(0,le);
    	        		}
    	        	}else if(volume.indexOf(".")>le){
    	        		volume=volume.substr(0,le)+volume.substr(volume.indexOf("."),volume.length-1);
    	        	}
    	        	$scope.sel.volume=parseFloat(volume);
        		}
	        	
        	}
        }
        $scope.checkMoney = function () {
        	if(!$scope.time.agent_money.$valid){
        		toastService.load('0',"请正确输入代收货款");
        		return;
        	}else{
        		var money=$scope.time.agent_money.$viewValue;
        		if(money!=""){
        			money = clearNoNum(money);
        			if(money.indexOf(".")>=0){
        				money=money.substr(0,money.indexOf(".")+3);
                	}
    	        	var le=7;
    	        	if(money.indexOf(".")<0){
    	        		if(money.length>le){
    	        			money=money.substr(0,le);
    	        		}
    	        	}else if(money.indexOf(".")>le){
    	        		money=money.substr(0,le)+money.substr(money.indexOf("."),money.length-1);
    	        	}
    	        	$scope.sel.agent_money=parseFloat(money);
        		}
	        	
        	}
        }
        $scope.checkInsured = function (){
        	if(!$scope.time.insured_money.$valid){
        		toastService.load('0',"请正确输入保价声明");
        		return;
        	}else{
        		var money=$scope.time.insured_money.$viewValue;
        		if(money!=""){
        			money = clearNoNum(money);
            		var le=7;
            		if(money.indexOf(".")>-1){
            			money=money.substr(0,money.indexOf("."));
            		}
            		if(money.length>le){
            			money=money.substr(0,le);
            		}
    	        	$scope.sel.insured_money=Number(money);
        		}
        		
        	}
        }
        
        
        $scope.select = function () {
            if ($scope.ionarrow == 'ic_ion-arrow-down-b') {
                $scope.myVal = false;
                $scope.ionarrow = 'ic_ion-arrow-up-b';
            } else {
                $scope.myVal = true;
                $scope.ionarrow = 'ic_ion-arrow-down-b';
            }
        };
        //点击查询结果中运费估算 进入查询界面
        $scope.select2 = function () {
        	$scope.myVal = false;
            $scope.ionarrow = 'ic_ion-arrow-up-b';
            $scope.priceModal.hide();
        };
        ///////////////////////
        business=2;
        citys($scope, $http,$location,$ionicModal,$ionicScrollDelegate,$rootScope);
        

    })
    /**货物追踪*/
    .controller('goodsTrackCtrl', function ($scope,$stateParams,$document,$ionicModal,$http,$location,$rootScope) {
    	$scope.billNo=$stateParams.id;
    	$scope.orderTrack=[];
    	$scope.orderTrackNull="";
    	$scope.ol=0;
    	$scope.waybill={};
    	$scope.switchType = function(type){
    		
            if(type == 1){
            	$scope.orderTrackNull=false;
                $scope.switch1 = 'dp-map-switch';
                $scope.switch2 = '';
                $scope.ddgz=true;
                $scope.other=false;//201211081
                $scope.isShow=true;
                $http.post('../WayBillNo/getGoodsTrace.jspa',{"keyword":$scope.billNo}).success(function (json) {
                	$scope.isShow=false;
                	if(json!=null){
                    	var detail = eval("(" + json.detail + ")");
                    	 if(detail!=null&&detail != ""&&detail.code=='10000'){
                    		 $scope.orderTrack=detail.orderTrack;
                    		 $scope.ol=detail.orderTrack.length;
                    	 }else {
                    		 $scope.orderTrackNull=true;
                         }
                	}else{
                		 $scope.orderTrackNull=true;
                	}
                });
            }else{
                $scope.orderTrackNull=false;
            	$scope.isShow=true;
                $http.post('../WayBillNo/getWayBillNo.jspa',{"keyword":$scope.billNo}).success(function (json) {
                	$scope.isShow=false;
                	$scope.switch1 = '';
                    $scope.switch2 = 'dp-map-switch';
                    $scope.ddgz=false;
                    $scope.other=true;
                	if(json!=null){
                    	var detail = eval("(" + json.detail + ")");
                    	if(detail==null||detail.code!='10000'){
                    		$scope.orderTrackNull=true;
                    		return;
                    	}
                    	 $scope.waybill=detail.detail;
                    	 /*if(detail != ""&&detail.code=='10000'){
                    		 $scope.orderTrack=detail.orderTrack;
                    		 $scope.ol=detail.orderTrack.length;
                    	 }else {
                    		 $scope.orderTrackNull=true;
                         }*/
                	}else{
                		 $scope.orderTrackNull=true;
                	}
                });
            }
    	}
    	$scope.switchType(1);
    	$scope.check=function(){  check($scope,$http,$location,$rootScope);}
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}

      $scope.check3=function(){
          $rootScope.goBackNot = true;
          $location.path("/goodSelect");
      }
      $scope.check4=function(){
          $rootScope.goBackNot = true;
          $location.path("/queryBranches");
      }
      $scope.check5=function(){
          $rootScope.goBackNot = true;
          $location.path("/realTimePrice");
      }
      $scope.check6=function(){
          $rootScope.goBackNot = true;
          $location.path("/expressDispatch");
      }
      $scope.goHome=function(){
          $rootScope.goBackNot = true;
          $location.path("#");
      }
    })
     .filter('rawHtml', ['$sce', function($sce){
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    }])

;
