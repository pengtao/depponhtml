angular.module('starter.loginControllers', [])
    //登陆页面
    .controller('LoginCtrl', function ($scope, $http, $location, $rootScope,userNameFac) {
        if (localStorage.userName != null) {
            $scope.userName = localStorage.userName;
        }
        if (localStorage.addCookie != null) {
            $scope.addCookie = localStorage.addCookie;
        }
        if (userNameFac.getCurrentUser() !== undefined) {
            $scope.userName = userNameFac.getCurrentUser();
        }
        $scope.loginGo = function () {
        
            $scope.errorText = "";
            try {
                if ($scope.addCookie == true || $scope.addCookie == "true") {
                    localStorage.userName = $scope.userName;
                    localStorage.addCookie = $scope.addCookie;
                } else {
                    localStorage.userName = "";
                    localStorage.addCookie = "";
                }
            } catch (e) {
            }
            if ($scope.userName != null && $scope.password != null) {
                //通过Ajax获取信息1
                var obj = {"userName": $scope.userName, "password": baseMEncode($scope.password)};
                //显示加载框
                $scope.isShow=true;
                $http.post('../login/getStatus.jspa', obj).success(function (json) {
                	$scope.isShow=false;//隐藏加载框
                    var detail = eval("(" + json.detail + ")");
                    if(detail==""||detail=="null"||detail===undefined||detail.exceptionCode=="S000099"){
                    	$scope.errorText="网络开小差，请检查后再试吧";
                    }else{
                    if (detail.code == '10000') {
                    	
                        //登录成功返回上次的页面
                        if ($rootScope.history == '/login/myDeppon') {
                            $location.path("/myDeppon");
                        } else if ($rootScope.history == '/login/placeOrder') {
                        	$rootScope.goBackNot=false;
                            $location.path("/placeOrder");
                        } else if ($rootScope.history == '/login/personInformation') {
                            $location.path("/personInformation");
                        } else {
                            $location.path("/home");
                        }
                    } else if(detail.code=="20017"){
                        $scope.errorText = "提示：亲，您的密码输错了哦，请重新输入！";
                    } else if(detail.code="20009"){
                        $scope.errorText = "提示：亲，您输入的用户名不存在哦，请重新输入！";
                    } else {
                        $scope.errorText="网络开小差，请检查后再试吧";
                    }
                    }
                })
            } else {
                $scope.errorText = "提示：亲，您还没有填写用户名或密码哦！";
            }
        };
        $scope.check = function () {
            check($scope, $http, $location,$rootScope);
        }
        $scope.check2 = function () {
            check2($scope, $http, $location,$rootScope);
        }
    })

    //注册页面
    .controller('RegisterCtrl', function ($scope, $timeout,$http,$location,$ionicModal,$rootScope,$ionicPopup,$ionicScrollDelegate,toastService,userNameFac) {
        $rootScope.goBackNot= false;
        $scope.errorText="";
        var phoneValid = false;
        $scope.checkPhone= function(){
            if($scope.phone=="" |$scope.phone===undefined |!$scope.register.phone.$valid){
                $scope.errorText="提示：亲，您的手机号有问题哦！";
            }else{
                $http.post('../login/validatePhone.jspa',{"phone":$scope.phone}).success(function (json) {
                    var detail = eval("(" + json.detail + ")");
                    if(detail.code=="20001"){
                        $scope.errorText="提示：亲，该手机号已被注册哦！";
                    }else{
                        $scope.errorText="";
                    }
                })
            }
        }
        $scope.checkCd= function(){
            if($scope.validateCode==""|$scope.validateCode===undefined| !$scope.register.validateCode.$valid){
                $scope.errorText="提示：亲，请先输入验证码哦";
            }else{
                $scope.errorText="";
            }
        }
        $scope.checkPassword= function(){
            if($scope.password==""|$scope.password===undefined|!$scope.register.password.$valid){
                $scope.errorText="提示：亲，请输入6到16位密码，不要包含字母和数字以外的字符哦";
            }else{
                $scope.errorText="";
            }
        }
        $scope.checkPw= function(){
            if($scope.password==""||$scope.password===undefined||!$scope.register.pw.$valid){
                $scope.errorText="提示：亲，请输入6到16位密码，不要包含字母和数字以外的字符哦";
            }else if($scope.password!=$scope.pw){
                $scope.errorText="提示：密码不一致，请核实！";
            }else{
                $scope.errorText="";
            }
        };

        $scope.registerGo = function() {
            if($scope.phone==null) {
                $scope.errorText="提示：信息不能为空!";
                $timeout(function () {
                    $scope.errorText="";
                }, 5000);
            }else{
                if ($scope.register.$valid) {
                    var obj = {"phone": $scope.phone, "validateCode": $scope.validateCode, "password": baseMEncode($scope.password)};
                    $scope.isShow=true;//显示加载框
                    $http.post('../login/getRegister.jspa', obj).success(function (json) {
                    	$scope.isShow=false;;//隐藏加载框
                        var detail = eval("(" + json.detail + ")");
                        if(detail==""||detail=="null"||detail===undefined){
                        	$scope.errorText="网络开小差，请检查后再试吧";
                        }else{
                        if (detail.code == '10000') {
                            userNameFac.setCurrentUser($scope.phone);
                        	toastService.load('1',"恭喜您已注册成功！");//显示提示框
                        	$location.path("/login/register");
//                            $ionicLoading.show({
//                                template: '<div style="background:#ffffff;padding:15px;text-align: center;"><img src="img/succeedRegister.png" style="width:35%;margin-bottom: 10px"/><p style="color:#171723;font-size: 15px;font-weight: bold">恭喜您注册成功啦!</p></div>'
//                            });
//                            $timeout(function() {
//                                $ionicLoading.hide();
//                                $location.path("/login/register");
//                            }, 1000);
                        	
                        	
                        } else if(detail.code == '20001') {
                            $scope.errorText = "提示：亲，该手机号已被注册哦！";
                        }else if(detail.code == '20006'||detail.code == '20007') {
                            $scope.errorText = "提示：亲，您的验证码输错了哦，请检查后重新输入";
                        }else if(detail.code == '20008'){
                            $scope.errorText = "提示：您的短信验证码已失效，请重新获取";
                        }else{
                        	toastService.load('0',"注册失败，请稍后重试！");//显示提示框
                        }
                        }
//                        $timeout(function () {
//                            $scope.errorText = "";
//                        }, 5000);
                    })
                }
            }
        };
        $scope.codeText = "获取验证码";
        $scope.timeLength = 60;
        $scope.checkcode = function (event) {
            $http.post('../login/validatePhone.jspa',{"phone":$scope.phone}).success(function (json) {
                var detail = eval("(" + json.detail + ")");
                if(detail==""||detail=="null"||detail===undefined){
                	return
                }else{
                if(detail.code!=="20001") {
                    event.target.disabled = true;
                    if ($scope.phone === undefined || $scope.phone == null||$scope.register.phone.$invalid) {
                        $scope.errorText = "提示：请输入手机号！";
                        event.target.disabled = false;
                        $scope.codeText = '获取验证码';
                        $scope.timeLength = 60;
                    } else {
                            $scope.phone = $scope.register.phone.$viewValue;
                            var obj = {"phone": $scope.phone, "codeType": '0'};
                            $http.post('../login/obtainMessageCode.jspa', obj).success(function (json) {
                            });
                        var t = function () {
                            $scope.timeLength--;
                            $scope.codeText = $scope.timeLength + '秒后重新发送';
                            //倒计时结束后
                            if ($scope.timeLength == 0) {
                                $timeout.cancel();
                                $scope.timeLength = 60;
                                event.target.disabled = false;
                                $scope.codeText = '重新获取';
                                return;
                            }
                            $timeout(t,1000);
                        }
                        $timeout(t,1000);
                    }
                } else{
                    $scope.errorText="提示：亲，该手机号已被注册哦！！";
                }
                }
                })
        };
        $rootScope.controlSystemBackButton =false;
        /*德邦协议*/
        $ionicModal.fromTemplateUrl('templates/loginAndRegister/depponAgree.html', {
            scope: $scope,
            animation: 'slide-in-right'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.openDeppon = function() {
            $rootScope.controlSystemBackButton=true;
            $scope.modal.show();
        };
        $scope.closeDeppon = function() {
            $rootScope.controlSystemBackButton = false;
            $scope.modal.hide();
        };
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });
        $scope.$on('$locationChangeStart', function(e) {
            if($rootScope.controlSystemBackButton){
                $scope.modal.hide();
                $rootScope.controlSystemBackButton = false;
                if($rootScope.goBackNot){
                    $location.path($location.path());
                }else{
                    //拦截系统返回按钮
                    e.preventDefault();

                }
            }
        });
        $scope.$on('modal.shown',function(){
            $ionicScrollDelegate.scrollTo(0,0);
        })
        $scope.check=function(){  check($scope,$http,$location,$rootScope);};
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);};
        $scope.check3=function(){
            $rootScope.goBackNot = true;
            $location.path("/goodSelect");
        };
        $scope.check4=function(){
            $rootScope.goBackNot = true;
            $location.path("/queryBranches");
        };
        $scope.check5=function(){
            $rootScope.goBackNot = true;
            $location.path("/realTimePrice");
        };
        $scope.check6=function(){
            $rootScope.goBackNot = true;
            $location.path("/expressDispatch");
        };
        $scope.goHome=function(){
            $rootScope.goBackNot = true;
            $location.path("#");
        };
    })
;