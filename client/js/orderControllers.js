///下单
function orders($scope, $http,$location,$ionicModal,$ionicScrollDelegate,$rootScope,toastService){
	$rootScope.goBackNot=false;
	$scope.nullOrder;
    $scope.orderEntity={};
    $scope.errorText;
    if($rootScope.orderEntity!=null){
    	$scope.orderEntity=$rootScope.orderEntity;
    	$scope.myVal = $scope.orderEntity.myVal;
        $scope.ionarrow = $scope.orderEntity.ionarrow;
        $rootScope.orderEntity=null;
        $scope.closePlaceModal =function(){
        	if($scope.orderEntity.myValBack == "1"){
        		//返回订单详情页面判断
        		$location.path("/myOrderDetail/"+$scope.orderEntity.orderNumber+"/"+$scope.orderEntity.orderStatus);
        	}else{
        		if($scope.myVal){//发货人管理
            		$location.path("/senderMng/1");
            	}else{//收货人管理
            		$location.path("/senderMng/2");
            	}
        	}
        	
    	}
    }else{
    	$scope.closePlaceModal =function(){
    		history.go(-1);
//    		$location.path("/home");
    	}
    }
    var error="亲，您的手机号有问题哦，请输入正确的手机号";//"提示：请输入正确的手机号";
    var errorname="亲，您填写的信息不全，需要补充哦";//"提示：请完善您的发货人信息";
    $scope.isShow=true;//显示加载框
    $http.post('../login/getSessionInfo.jspa').success(function (json) {
    	 $scope.isShow=false;
        $scope.user =json.userRest;
        if($scope.user==null){
        	$location.path("/login/placeOrder");
        }
    });
    $scope.check=function(){  check($scope,$http,$location,$rootScope);}
    $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
    $scope.check3=function(){
        $rootScope.goBackNot = true;
        $location.path("/goodSelect");
    }
    $scope.check4=function(){
        $rootScope.goBackNot = true;
        $location.path("/queryBranches");
    }
    $scope.check5=function(){
        $rootScope.goBackNot = true;
        $location.path("/realTimePrice");
    }
    $scope.check6=function(){
        $rootScope.goBackNot = true;
        $location.path("/expressDispatch");
    }
    $scope.goHome=function(){
        $rootScope.goBackNot = true;
        $location.path("#");
    }
    $scope.select = function () {
        if ($scope.ionarrow == 'ic_ion-arrow-down-b') {
            $scope.myVal = false;
            $scope.ionarrow = 'ic_ion-arrow-up-b';
        } else {
            $scope.myVal = true;
            $scope.ionarrow = 'ic_ion-arrow-down-b';
        }
    };
    $scope.testTel=function(index){
        var v=$scope.orderEntity.contactMobile;
        if(index==2){
            v=$scope.orderEntity.receiverCustMobile;
        }
        var pattern=/^[\d]{8}$/;
        if(v!=null&&v!=""){
            if(!pattern.test(v)){
                var pattern2=/^1[34578]\d{9}$/;
                if(!pattern2.test(v)){
                    $scope.errorText=error;
                }else{
                    $scope.errorText='';
                }
            }else{
                $scope.errorText='';
            }
        }
    }
    location();//进入定位
    /*下单成功*/
    $rootScope.controlSystemBackButtonOrderSuccess=false;
    $ionicModal.fromTemplateUrl('templates/order/placeOrderSuccess.html', {
        scope: $scope,
        animation: 'slide-in-right'
    }).then(function(modal) {
        $scope.modalOrderSuccess = modal;
    });
    $scope.openOrderSuccessModal = function() {
    	$rootScope.controlSystemBackButtonOrderSuccess=true;
    	$scope.modalOrderSuccess.show();
    }
    $scope.placeorderMethod = function(){
        if( $scope.orderEntity.shipperName==null|| $scope.orderEntity.shipperName==""|| $scope.orderEntity.contactMobile==null|| $scope.orderEntity.contactMobile==""
            ||$scope.orderEntity.contactCity==null|| $scope.orderEntity.contactCity==""||$scope.orderEntity.contactAddress==null|| $scope.orderEntity.contactAddress==""){
            $scope.errorText=errorname;
            return;
        }
        $scope.testTel(1);
        if($scope.errorText!=''){
            return;
        }
        $scope.errorText='';
        var orderEntity={"shipperName":$scope.orderEntity.shipperName,"shipperPhone":$scope.orderEntity.contactMobile,
                          "saddress":$scope.orderEntity.contactCity+","+$scope.orderEntity.contactAddress,
                          "conName":$scope.orderEntity.receiverCustName,"conPhone":$scope.orderEntity.receiverCustMobile,
                          "caddress":$scope.orderEntity.receiverCustCity+","+$scope.orderEntity.receiverCustAddress,
                           "remark":$scope.orderEntity.remark};
        $scope.isShow=true;//显示加载框
        $http.post('../order/createOrder.jspa',orderEntity).success(function (json) {
        	 $scope.isShow=false;
            var detail = eval("(" + json.detail + ")");
            if(detail.code=='10000'){
            	$scope.openOrderSuccessModal();
            	//$location.path("/placeOrderSuccess");
            }else{
            	toastService.load('0',"下单失败");//显示提示框
            }
        }).error(function (data, status, headers, config) {
        	toastService.load('0',"下单失败");//显示提示框
        }).finally(function () {
            // Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        });
    }
    var openI;
    $scope.receiveTitle="收货人";
    /*收货人选择器*/
    $rootScope.controlSystemBackButtonReceiver=false;
    $ionicModal.fromTemplateUrl('templates/receiveList/receive-modal.html', {
        scope: $scope,
        animation: 'slide-in-right'
    }).then(function(modal) {
        $scope.modalReceiver = modal;
    });
    var pageSize=10;
	var pageNum=1;
    $scope.openReceiverModal = function(oI,pageNum) {
    	$scope.isShow=true;//显示加载框
    	$http.post('../login/getSessionInfo.jspa').success(function (json) {
    		$scope.isShow=false;
            $scope.user =json.userRest;
            if($scope.user!=null){
            	openI=oI;
            	var url="";
            	if(openI==1){
            		$scope.sendT=true;
            		$scope.receiveT=false;
                	$scope.receiveTitle="发货人";
                	url="../sender/obtainConsignorList.jspa";//?userName="+$scope.user.userName+"&pageSize="+pageSize+"&pageIndex="+pageNum;
                }else{
                	$scope.sendT=false;
            		$scope.receiveT=true;
                	$scope.receiveTitle="收货人";
                	url="../receiver/getReceiveDetail.jspa";//?pageSize="+pageSize+"&pageIndex="+pageNum;
                }
            	$scope.isShow=true;//显示加载框
            	$scope.moreDataCanBeLoaded=true;
            	var userss=[];
                $http.post(url,{"pageSize":pageSize,"pageIndex":pageNum}).success(function (json) {
                	$scope.isShow=false;
                    var detail = eval("(" + json.detail + ")");
                    if(detail.code=='10000'){
                    	if(openI==1){
                    		userss=detail.consignors;
                        }else{
                        	userss=detail.consigneeList;
                        }
                    	if(pageNum==1){
                			$scope.users=userss;
                		}else{
                			$scope.users=$scope.users.concat(userss);
                		}
                    }else{
                    	$scope.nullUser=true;
                    	/*if(openI==1){
                    		$scope.nulluserT="您还没有添加发货人哦";
                    	}else{
                    		$scope.nulluserT="您还没有添加收货人哦";
                    	}*/
                    }
                    if(userss.length<pageSize){
                    	$scope.moreDataCanBeLoaded = false;
                    }else{
                    	$scope.$broadcast('scroll.infiniteScrollComplete');
                    }
                }).error(function (data, status, headers, config) {
                	// 如果请求失败，关闭加载更多
                	$scope.moreDataCanBeLoaded = false;
                });
                if(pageNum==1){
                	$rootScope.controlSystemBackButtonReceiver=true;
                    $scope.modalReceiver.show();
                }
            }else{
            	$location.path("/login/placeOrder");
            }
    	});
    	
    };
 // 上拉加载更多
    $scope.loadReceiveModalMore = function () {
    	if($scope.users==null||$scope.users.length==0){
    		return;
    	}
        $scope.moreDataCanBeLoaded = false;
        pageNum++;
        $scope.openReceiverModal(openI,pageNum);
    };
    $scope.hasMoreData=function(){
    	return $scope.moreDataCanBeLoaded;
    }
    $scope.recieverGo=function(name,phone,area,address){
    	if(openI==1){
    		$scope.orderEntity.shipperName=name;
            $scope.orderEntity.contactMobile=Number(phone);
            $scope.orderEntity.contactCity=area;
            $scope.orderEntity.contactAddress=address;
    	}else{
    		 $scope.orderEntity.receiverCustName=name;
             $scope.orderEntity.receiverCustMobile=Number(phone);
             $scope.orderEntity.receiverCustCity=area;
             $scope.orderEntity.receiverCustAddress=address;
    	}
        $rootScope.controlSystemBackButtonReceiver=false;
        $scope.modalReceiver.hide();
    }
    $scope.closeReceiverModal = function() {
        $scope.modalReceiver.hide();
    };
    $scope.closeOrderSuccessModal = function() {
    	$scope.orderEntity={};
        $scope.modalOrderSuccess.hide();
    };
    $scope.myNewOrder = function() {
    	$rootScope.controlSystemBackButtonOrderSuccess=false;
    	$rootScope.goBackNot=true;
    	$location.path("/myOrder");
    };
	$scope.$on('$destroy', function() {
        $scope.modalReceiver.remove();
        $scope.modalOrderSuccess.remove();
    });
    $scope.$on('$locationChangeStart', function(e) {
        if($rootScope.controlSystemBackButtonReceiver){
            $scope.modalReceiver.hide();
            $rootScope.controlSystemBackButtonReceiver = false;
            if($rootScope.goBackNot){
                $location.path($location.path());
            }else{
                //拦截系统返回按钮
                e.preventDefault();

            }
        }
        if($rootScope.controlSystemBackButtonOrderSuccess){
        	$scope.modalOrderSuccess.hide();
            $rootScope.controlSystemBackButtonOrderSuccess = false;
            if($rootScope.goBackNot){
                $location.path($location.path());
            }else{
                //拦截系统返回按钮
                e.preventDefault();

            }
        }
    });
    ////////////
    business=1;
    citys($scope, $http,$location,$ionicModal,$ionicScrollDelegate,$rootScope);

    //////////////////////// // 百度地图API功能 定位当前位置
    $scope.latitude="";
    $scope.longitude="";
    $scope.errorDenied="";
    var myGeo = new BMap.Geocoder();
    var indd=1;
    var ind=0;
    function location()
    {
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(showPosition,showError,{
            	enableHighAccuracy: true, //是否启用高精确度模
            	timeout:30000//30秒
            });
        }
        else{
        	$scope.isShow=false;
        	indd=3;
        	$scope.errorDenied="位置获取不到，请手动输入！";
        }
    }
    function showError(error)
    {   
    	indd=3;
    switch(error.code)
      {
      case error.PERMISSION_DENIED:
    	  $scope.errorDenied="位置获取不到，请手动输入！";
        break;
      case error.POSITION_UNAVAILABLE:
    	  $scope.errorDenied="位置获取不到，请手动输入！";
        break;
      case error.TIMEOUT:
    	  $scope.errorDenied="位置获取不到，请手动输入！";
        break;
      case error.UNKNOWN_ERROR:
    	  $scope.errorDenied="位置获取不到，请手动输入！";
//    	  toastService.load('0',"位置获取不到，请手动输入！");
        break;
      }
    }
    function showError2(error)
    {   $scope.isShow=false;
    	indd=3;
    switch(error.code)
      {
      case error.PERMISSION_DENIED:
    	  $scope.errorDenied="位置获取不到，请手动输入！";
        break;
      case error.POSITION_UNAVAILABLE:
    	  $scope.errorDenied="位置获取不到，请手动输入！";
        break;
      case error.TIMEOUT:
    	  $scope.errorDenied="位置获取不到，请手动输入！";
        break;
      case error.UNKNOWN_ERROR:
    	  $scope.errorDenied="位置获取不到，请手动输入！";
//    	  toastService.load('0',"位置获取不到，请手动输入！");
        break;
      }
    loocation(ind);
    }
    function showPosition(position)
    {   
    	indd=2;
        var lat = parseFloat(position.coords.latitude);
        var lng = parseFloat(position.coords.longitude);
//        alert(lat+"--"+lng);
        var po = new BMap.Point(lng, lat);
        myGeo.getLocation(po, function (rs) {
            if (rs) {
            	var addComp = rs.addressComponents;
            	var ssq=addComp.province +"-"+ addComp.city +"-"+ addComp.district;
            	var str=addComp.street + addComp.streetNumber;
            	$scope.latitude=ssq;
                $scope.longitude=str;
            }else{
            	$scope.errorDenied="位置获取不到，请手动输入！";
            }
        });
    }
    function showPosition2(position)
    {   $scope.isShow=false;
    	indd=2;
        var lat = parseFloat(position.coords.latitude);
        var lng = parseFloat(position.coords.longitude);
//        alert(lat+"--"+lng);
        var po = new BMap.Point(lng, lat);
        myGeo.getLocation(po, function (rs) {
            if (rs) {
            	var addComp = rs.addressComponents;
            	var ssq=addComp.province +"-"+ addComp.city +"-"+ addComp.district;
            	var str=addComp.street + addComp.streetNumber;
            	$scope.latitude=ssq;
                $scope.longitude=str;
            }else{
            	$scope.errorDenied="位置获取不到，请手动输入！";
            }
            loocation(ind);
        });
    }
    function loocation(){
    	if($scope.errorDenied!=""){
			toastService.load('0',"位置获取不到，请手动输入！");
			return;
		}
		if(ind==1){
            $scope.$apply($scope.orderEntity.contactCity=$scope.latitude);
            $scope.$apply($scope.orderEntity.contactAddress=$scope.longitude);
        }else{
        	$scope.$apply($scope.orderEntity.receiverCustCity=$scope.latitude);
            $scope.$apply($scope.orderEntity.receiverCustAddress=$scope.longitude);
        }
    }
    $scope.getLocation = function(inde){
    	ind=inde;
    	if(indd!=1){
    		loocation();
    	}else{
    		$scope.isShow=true;//显示加载框
    		if (navigator.geolocation)
            {
                navigator.geolocation.getCurrentPosition(showPosition2,showError2,{
                	enableHighAccuracy: true, //是否启用高精确度模
                	timeout:30000//30秒
                });
            }
            else{
            	$scope.isShow=false;
            	indd=3;
            	$scope.errorDenied="位置获取不到，请手动输入！";
            	toastService.load('0',"位置获取不到，请手动输入！");
            }
    	}
        
    }
}

angular.module('starter.orderControllers', [])
     /**我的订单*/
    .controller('myOrderCtrl', function ($scope, $http,$location,$rootScope,waybillNumberFac) {
        $scope.myorders = [];
        $scope.area={};
        $scope.pageSize=5;
        $scope.pageNum=1;
        $scope.nullOrder;
        $scope.selectMyOrders = function (param) {
        	 $scope.isShow=true;//显示加载框
            //通过Ajax获取信息
        	$http.post('../login/getSessionInfo.jspa').success(function (json) {
        		 $scope.isShow=false;
                $scope.user =json.userRest;
                if($scope.user!=null){
                	if(param==0){
                		$scope.pageNum=1;
                	}
		        	var orderList={"keyword":$scope.area.name,"pageSize":$scope.pageSize,"pageIndex":$scope.pageNum};
		        	$scope.isShow=true;//显示加载框
		        	$scope.moreDataCanBeLoaded=true;
		            $http.post('../order/queryOrderList.jspa',orderList).success(function (json) {
		        		$scope.isShow=false;
		                if (json==null || json==""||json.detail==null||json.detail==""||json.detail.length==0) {
		                    // 如果没有更多信息，关闭加载更多
		                    $scope.nullOrder=true;
                            $scope.loadMoreWord="";
		                    $scope.myorders = [];
		                    $scope.moreDataCanBeLoaded = false;
		                    return;
		                }
		                var detail = eval("(" + json.detail + ")");
		                if(detail.code=='10000'){
		                	if(param==0){
			                	$scope.myorders=detail.orders;
			                }else{
			                	$scope.myorders=$scope.myorders.concat(detail.orders);
			                }
			                if(detail.orders.length<$scope.pageSize){
			                	$scope.moreDataCanBeLoaded = false;
                                $scope.loadMoreWord="";
			                }else{
                                $scope.loadMoreWord="上滑查看更多";
			                	 $scope.$broadcast('scroll.infiniteScrollComplete');
			                }
		                }else{
		                	$scope.moreDataCanBeLoaded = false;
		                	$scope.nullOrder=true;
		                    $scope.myorders = [];
		                }
		                
		            }).error(function (data, status, headers, config) {
		                // 如果请求失败，关闭加载更多
		                $scope.moreDataCanBeLoaded = false;
		            });
                }else{
                	$location.path("/login/myDeppon");
                }
        	});
        }
        $scope.selectMyOrders(0);
        // 上拉加载更多
        $scope.loadMore = function () {
        	if($scope.myorders==null||$scope.myorders.length==0){
        		return;
        	}
            $scope.moreDataCanBeLoaded = false;
            $scope.pageNum++;
            $scope.selectMyOrders(1);
        };
        $scope.hasMoreData=function(){
        	return $scope.moreDataCanBeLoaded;
        }
        $scope.myOrderList = function(index,orderNumber,orderStatus,waybillNumber){
        	$rootScope.goBackNot=true;
            if(waybillNumber!=null&&waybillNumber!=""&&waybillNumber!=undefined){
                waybillNumberFac.setWaybill(waybillNumber);
            }
        	$location.path("/myOrderDetail/"+orderNumber+"/"+orderStatus);
        }

        $scope.check=function(){  check($scope,$http,$location,$rootScope);}
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
    })
    
    /**发货人管理*/
    .controller('senderMngCtrl', function ($scope, $stateParams,$http,$location,$ionicModal,$rootScope,$ionicScrollDelegate,toastService,showConfirm) {
    	$rootScope.goBackNot=false;
    	$scope.param=$stateParams.param;//
    	if($scope.param==1){//发货人
        	$scope.senderT=true;
        	$scope.receiverT=false;
        	$scope.senderTitle="发货人";
        }else{//收货人
        	$scope.senderT=false;
        	$scope.receiverT=true;
        	$scope.senderTitle="收货人";
        }
    	$scope.usersenders=[];
        $scope.pageSize=10;
        $scope.pageNum=1;
        $scope.selectSender = function (p) {
        	$scope.isShow=true;//显示加载框
        	$http.post('../login/getSessionInfo.jspa').success(function (json) {
        		$scope.isShow=false;//隐藏加载框
                $scope.user =json.userRest;
                if($scope.user!=null){
                	var url="";
                    if($scope.param==1){
                    	url="../sender/obtainConsignorList.jspa";
                    }else{
                    	url="../receiver/getReceiveDetail.jspa";
                    }
                    var orusername={"pageSize":$scope.pageSize,"pageIndex":$scope.pageNum};
		        	$scope.isShow=true;//显示加载框
                    $scope.moreDataCanBeLoaded = true;
                	$http.post(url,orusername).success(function (json) {
                		$scope.isShow=false;//显示加载框
                        var detail = eval("(" + json.detail + ")");
                        if (json==null || json==""||json.detail==null||json.detail==""||json.detail.length==0) {
                            // 如果没有更多信息，关闭加载更多
                            $scope.loadMoreWord="";
                            $scope.nullUsersender=true;
                            $scope.usersenders = [];
                            $scope.moreDataCanBeLoaded = false;
                            return;
                        }
                        if($scope.param==1){
                            if(detail.consignors.length<5){
                               $scope.loadMoreWord="";
                            }else{
                               $scope.loadMoreWord="上滑查看更多";
                            }
                        }else{
                            if(detail.consigneeList.length<5){
                                $scope.loadMoreWord="";
                            }else{
                                $scope.loadMoreWord="上滑查看更多";
                            }
                        }
                        var userss=[];
                        if(detail.code=='10000'){
                        	if($scope.param==1){
                        		userss=detail.consignors;
                        	}else{
                        		userss=detail.consigneeList;
                        	}
                        	if($scope.pageNum==1){
                        		if(userss.length==0){
                        			$scope.loadMoreWord="";
                                	$scope.nullUsersender=true;
                        		}else{
                                	$scope.nullUsersender=false;
                        		}
                        	}
                        }else{
                            $scope.loadMoreWord="";
                        	$scope.nullUsersender=true;
                        }
                        if(p==1){
                        	$scope.usersenders=userss;
                        }else{
                        	$scope.usersenders=$scope.usersenders.concat(userss);
                        }
                        if(userss.length<$scope.pageSize){
                        	$scope.moreDataCanBeLoaded = false;
                        }else{
                        	$scope.$broadcast('scroll.infiniteScrollComplete');
                        }
                    }).error(function (data, status, headers, config) {
                        // 如果请求失败，关闭加载更多
                        $scope.moreDataCanBeLoaded = false;
                    });
                }else{
                	$location.path("/login/myDeppon");
                }
        	});
        }
        $scope.selectSender($scope.pageNum);
        // 上拉加载更多
        $scope.loadSenderMore = function () {
        	if($scope.usersenders==null||$scope.usersenders.length==0){
        		return;
        	}
            $scope.moreDataCanBeLoaded = false;
            $scope.pageNum++;
            $scope.selectSender($scope.pageNum);
        };
        $scope.hasMoreData=function(){
        	return $scope.moreDataCanBeLoaded;
        }
        
        ////发货人修改
        $rootScope.controlSystemBackButtonSEdit=false;
    	$ionicModal.fromTemplateUrl('templates/order/senderAdd.html', {
            scope: $scope,
            animation: 'slide-in-right'
        }).then(function (modal) {
            $scope.sendEditmodal = modal;
        });
    	$scope.consigneeInfo = {};
    	$scope.consignorInfo = {};
    	$scope.editAdd="修改";
    	$scope.add='';
        $scope.edit=function(pid){
        	$scope.editAdd="修改";
        	$scope.add='2';
        	$scope.isShow=true;//显示加载框
        	$http.post('../login/getSessionInfo.jspa').success(function (json) {
        		$scope.isShow=false;//隐藏加载框
                $scope.user =json.userRest;
                if($scope.user!=null){
                	if($scope.param==1){
                		var name=document.getElementById("oname"+(pid)).innerHTML;
                    	var phone=document.getElementById("ophone"+(pid)).innerHTML;
                    	var area=document.getElementById("ocitypro"+(pid)).innerHTML;
                    	var address=document.getElementById("ocityaddre"+(pid)).innerHTML;
                		$scope.consignorInfo.id=pid;
                		$scope.consignorInfo.consignorName=name;
                		$scope.consignorInfo.consignorPhone=phone==""?"":Number(phone);
                		$scope.consignorInfo.addrProCity=area;
                		$scope.consignorInfo.consignorAddress=address;
                	}else{
                		var name=document.getElementById("ename"+(pid)).innerHTML;
                    	var phone=document.getElementById("ephone"+(pid)).innerHTML;
                    	var area=document.getElementById("ecitypro"+(pid)).innerHTML;
                    	var address=document.getElementById("ecityaddre"+(pid)).innerHTML;
                		$scope.consigneeInfo.id=pid;
                		$scope.consigneeInfo.consigneeName=name;
                		$scope.consigneeInfo.cellPhone=phone==""?"":Number(phone);
                		$scope.consigneeInfo.addrProCity=area;
                		$scope.consigneeInfo.address=address;
                	}
                	$rootScope.controlSystemBackButtonSEdit=true;
                	if($scope.param==1){
                    	business=6;
                    }else{
                    	business=5;
                    }
                    citys($scope, $http,$location,$ionicModal,$ionicScrollDelegate,$rootScope);
            		$scope.sendEditmodal.show();
                }else{
                	$location.path("/login/myDeppon");
                }
        	});
        }
        $scope.xzMethod=function(){
        	$scope.editAdd="新增";
        	$scope.add='1';
        	$scope.isShow=true;//显示加载框
        	$http.post('../login/getSessionInfo.jspa').success(function (json) {
        		$scope.isShow=false;//隐藏加载框
                $scope.user =json.userRest;
                if($scope.user!=null){
                	//收发货人信息  带入  下单界面
                	if($scope.param==1){
                		$scope.consignorInfo.id="";
                		$scope.consignorInfo.consignorName="";
                		$scope.consignorInfo.consignorPhone="";
                		$scope.consignorInfo.addrProCity="";
                		$scope.consignorInfo.consignorAddress="";
                	}else{
                		$scope.consigneeInfo.id="";
                		$scope.consigneeInfo.consigneeName="";
                		$scope.consigneeInfo.cellPhone="";
                		$scope.consigneeInfo.addrProCity="";
                		$scope.consigneeInfo.address="";
                	}
                	$rootScope.controlSystemBackButtonSEdit=true;
                	if($scope.param==1){
                    	business=6;
                    }else{
                    	business=5;
                    }
                    citys($scope, $http,$location,$ionicModal,$ionicScrollDelegate,$rootScope);
                	$scope.sendEditmodal.show();
                }else{
                	$location.path("/login/myDeppon");
                }
        	});
        }
        $scope.closeSendEditModal = function() {
        	$rootScope.controlSystemBackButtonSEdit = false;
            $scope.sendEditmodal.hide();
        };
        $scope.$on('$locationChangeStart', function(e) {
            if($rootScope.controlSystemBackButtonSEdit){
                $scope.sendEditmodal.hide();
                $rootScope.controlSystemBackButtonSEdit = false;
                if($rootScope.goBackNot){
                    $location.path($location.path());
                }else{
                    //拦截系统返回按钮
                    e.preventDefault();

                }
            }
        });
        
        ///下单
    	$scope.orderEntity={};
        $scope.placeorder=function(pid){
               //收发货人信息  带入  下单界面
               if($scope.param==1){
            	    var name=document.getElementById("oname"+(pid)).innerHTML;
               		var phone=document.getElementById("ophone"+(pid)).innerHTML;
               		var area=document.getElementById("ocitypro"+(pid)).innerHTML;
               		var address=document.getElementById("ocityaddre"+(pid)).innerHTML;
                	$scope.orderEntity.myVal = true;
                    $scope.orderEntity.ionarrow = 'ic_ion-arrow-down-b';
                    $scope.orderEntity.shipperName=name;
                    $scope.orderEntity.contactMobile=phone==""?"":Number(phone);
                    $scope.orderEntity.contactCity=area;
                    $scope.orderEntity.contactAddress=address;
                        
                    $scope.orderEntity.receiverCustName="";
                    $scope.orderEntity.receiverCustMobile="";
                    $scope.orderEntity.receiverCustCity="";
                    $scope.orderEntity.receiverCustAddress="";
                }else{
                	var name=document.getElementById("ename"+(pid)).innerHTML;
                	var phone=document.getElementById("ephone"+(pid)).innerHTML;
                	var area=document.getElementById("ecitypro"+(pid)).innerHTML;
                	var address=document.getElementById("ecityaddre"+(pid)).innerHTML;
                	$scope.orderEntity.myVal = false;
                    $scope.orderEntity.ionarrow = 'ic_ion-arrow-up-b';
                    $scope.orderEntity.receiverCustName=name;
                    $scope.orderEntity.receiverCustMobile=phone==""?"":Number(phone);
                    $scope.orderEntity.receiverCustCity=area;
                    $scope.orderEntity.receiverCustAddress=address;
                        
                    $scope.orderEntity.shipperName="";
                    $scope.orderEntity.contactMobile="";
                    $scope.orderEntity.contactCity="";
                    $scope.orderEntity.contactAddress="";
                }
               $rootScope.orderEntity=$scope.orderEntity;
               $location.path("/placeOrder");
        }
        /*$scope.keyDownTop=function(){
        	console.log("down");
        	document.getElementById('MyDiveename').style.display="none";
        	document.getElementById('MyDiveetel').style.marginTop="0px";
        	document.getElementById('MyDiveecity').style.marginTop="50px";
        	document.getElementById('MyDiveeadd').style.marginTop="100px";
        }
        $scope.keyUpTop=function(){
        	console.log("up");
        	document.getElementById('MyDiveename').style.marginTop="15px";
        	document.getElementById('MyDiveetel').style.marginTop="63px";
        	document.getElementById('MyDiveecity').style.marginTop="111px";
        	document.getElementById('MyDiveeadd').style.marginTop="159px";
        }*/
        /////收发货人保存
        var error="提示：请输入正确的手机号";
        var errorname="提示：请输入收货人姓名";
        $scope.errorText="";
        $scope.testSendMobile=function(){
        	var v="";
        	if($scope.param==1){//发货人
        		v=$scope.consignorInfo.consignorPhone;
        	}else{
        		v=$scope.consigneeInfo.cellPhone;
        	}
            var pattern=/^[\d]{8}$/;
            if(v!=null&&v!=""){
                if(!pattern.test(v)){
                    var pattern2=/^1[34578]\d{9}$/;
                    if(!pattern2.test(v)){
                        $scope.errorText=error;
                    }else{
                        $scope.errorText='';
                    }
                }else{
                    $scope.errorText='';
                }
            }else{
            	if($scope.param==1){//发货人
            		$scope.errorText=error;
            	}else{
            		$scope.errorText='';
            	}
            }
            if($scope.errorText!=""){
            	return;
            }
        }
        $scope.addMethod = function () {
        	$scope.testSendMobile();
        	if($scope.errorText!=""){
            	return;
            }
        	var name="";
        	var scity="";
        	var saddress="";
        	var errorcity="";
        	var erroraddress="";
        	if($scope.param==1){//发货人
        		name=$scope.consignorInfo.consignorName;
        		scity=$scope.consignorInfo.addrProCity;
        		saddress=$scope.consignorInfo.consignorAddress;
        		errorname="提示：请输入发货人姓名";
        		errorcity="提示：请输入发货人城市";
        		erroradd="提示：请输入发货人详细地址";
        	}else{
        		name=$scope.consigneeInfo.consigneeName;
        		errorname="提示：请输入收货人姓名";
        	}
        	if(name==null||name==""){
        		$scope.errorText=errorname;
        	}else{
        		$scope.errorText='';
        	}
        	if($scope.errorText!=''){
                return;
            }
        	var url='';
        	var obj={};
        	if($scope.param==1){//发货人
        		if($scope.add==1){
        			url="../sender/addSender.jspa";
        		}else{
        			url="../sender/modifySender.jspa";
        		}
        		obj=$scope.consignorInfo;
        	}else{
        		if($scope.add==1){
        			url="../receiver/add.jspa";
        		}else{
        			url="../receiver/modifyReceiver.jspa";
        		}
        		obj=$scope.consigneeInfo;
        	}
        	$scope.isShow=true;//显示加载框
            $http.post(url,obj).success(function (json) {
            	$scope.isShow=false;//隐藏加载框
                var detail = eval("(" + json.detail+ ")");
                if(detail.code=="10000"){
                    $scope.closeSendEditModal();
                    if($scope.add==1){//新增成功后 重新加载
                    	toastService.load('1',"保存成功");
                    	$scope.pageNum=1;
                    	$scope.selectSender($scope.pageNum);
                    }else{
                    	toastService.load('1',"修改成功");
                    	//修改成功后 更新列表中数据
                    	var  objid=obj.id;
                    	if($scope.param==1){//发货人
                    		document.getElementById("oname"+(objid)).innerHTML=obj.consignorName;
                    		document.getElementById("ophone"+(objid)).innerHTML=obj.consignorPhone;
                    		document.getElementById("oarea"+(objid)).innerHTML=obj.addrProCity+obj.consignorAddress;
                    		document.getElementById("ocitypro"+(objid)).innerHTML=obj.addrProCity;
                        	document.getElementById("ocityaddre"+(objid)).innerHTML=obj.consignorAddress;
                    	}else{
                    		document.getElementById("ename"+(objid)).innerHTML=obj.consigneeName;
                    		document.getElementById("ephone"+(objid)).innerHTML=obj.cellPhone;
                    		document.getElementById("earea"+(objid)).innerHTML=obj.addrProCity+obj.address;
                    		document.getElementById("ecitypro"+(objid)).innerHTML=obj.addrProCity;
                        	document.getElementById("ecityaddre"+(objid)).innerHTML=obj.address;
                    	}
                    }
                    
                }else{
                	$scope.errorText=detail.message;
                }
            });
        }
        $scope.delSendMethod=function(vid){
            showConfirm.load("确定要删除吗？",function(){
                var url="";
                if($scope.param==1){
                    url="../sender/delete.jspa";
                }else{
                    url='../receiver/deleteReceiver.jspa';
                }
                $scope.isShow=true;//显示加载框
                $http.post(url,{"keyword":vid}).success(function (json) {
                    $scope.isShow=false;
                    var detail = eval("(" + json.detail+ ")");
                    if(detail.code=="10000"){
                    	toastService.load('1',"删除成功");
                    	if($scope.param==1){//发货人
                    		document.getElementById("e"+vid).style.display = "none";
                    	}else{
                    		document.getElementById("kk"+vid).style.display = "none";
                    	}
                    	$scope.pageNum=1;
                    	$scope.selectSender($scope.pageNum);
                    }else{
                        toastService.load('0',"删除失败！");//显示提示框
                    }
                }).error(function (data, status, headers, config) {
                    toastService.load('0',"删除失败");
                });
                return true;
            })
       }
        /*$scope.closePlaceModal = function() {
        	$rootScope.controlSystemBackButtonSender = false;
            $scope.senderPmodal.hide();
        };
        $scope.$on('$locationChangeStart', function(e) {
            if($rootScope.controlSystemBackButtonSender){
                $scope.senderPmodal.hide();
                $rootScope.controlSystemBackButtonSender = false;
                if($rootScope.goBackNot){
                    $location.path($location.path());
                }else{
                    //拦截系统返回按钮
                    e.preventDefault();

                }
            }
        });*/
        $scope.check=function(){  check($scope,$http,$location,$rootScope);}
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
        $scope.check3=function(){
            $rootScope.goBackNot = true;
            $location.path("/goodSelect");
        }
        $scope.check4=function(){
            $rootScope.goBackNot = true;
            $location.path("/queryBranches");
        }
        $scope.check5=function(){
            $rootScope.goBackNot = true;
            $location.path("/realTimePrice");
        }
        $scope.check6=function(){
            $rootScope.goBackNot = true;
            $location.path("/expressDispatch");
        }
        $scope.goHome=function(){
            $rootScope.goBackNot = true;
            $location.path("#");
        }
    })
    
    /**我要下单*/
    .controller('placeOrderCtrl', function ($scope, $http,$location,$ionicModal,$rootScope,$ionicScrollDelegate,toastService) {
    	$scope.myVal = true;
        $scope.ionarrow = 'ic_ion-arrow-down-b';
    	orders($scope, $http,$location,$ionicModal,$ionicScrollDelegate,$rootScope,toastService);
    	
    })
    
    

    /**我要下单*/
    .controller('addressCtrl', function ($scope, $http,$location) {
        // 百度地图API功能
        /*var myGeo = new BMap.Geocoder();
        $scope.demo='';
        $scope.address='';
        getLocation();
        function getLocation()
        {
            if (navigator.geolocation)
            {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
            else{$scope.demo='Geolocation is not supported by this browser.';}
        }
        function showPosition(position)
        {
            $scope.address=position.coords.longitude+','+position.coords.latitude;
            geoParse($scope.address);
        }
        function geoParse(str) {
            str = str.toString();
            //第一个值为纬度并转化为float类型
            var lat = parseFloat(str[1]);
            //第二个值为经度并转化为float类型
            var lng = parseFloat(str[0]);
            if (lat == 0 || lng == 0 || isNaN(lat) || isNaN(lng)) return false;
            var po = new BMap.Point(lng, lat);
            myGeo.getLocation(po, function (rs) {
                if (rs) {
                   var str = lng + "," + lat + "：<br/>" + rs.address ;
                   console.log(str);
                   $scope.demo=str;
                }
            });
        }*/
    })

;
