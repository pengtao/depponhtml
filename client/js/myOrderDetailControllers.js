angular.module('starter.myOrderDetailControllers', [])
    /*我的订单详情*/
    .controller('myOrderDetailCtrl', function ($scope, $stateParams, $http, $location, $ionicModal, $ionicScrollDelegate, $rootScope, waybillNumberFac,toastService,showConfirm) {
        orders($scope, $http, $location, $ionicModal, $ionicScrollDelegate, $rootScope);

        $scope.panduan = function (obj) {
            if (obj == "null" || obj == "undefined" || obj == "" || obj == "0") {
                obj = "";
            } else {
                obj = obj;
                return obj;
            }
        }


        $scope.orderNumber = $stateParams.id;
        $scope.orderStatus = $stateParams.Status;


        var nu = {"orderNumber": $scope.orderNumber};

        //订单 受理状态判断
        function orderPan() {
            if ($scope.orderStatus == "WAIT_ALLOT" || $scope.orderStatus == "WAIT_ACCEPT") {
                $scope.opac = "button b1";//已受理  不可操作
            } else {
                $scope.opac = "button b2";// 可操作 高亮显示
            }
        }

        orderPan();
        $scope.backOrder = function (opac) {
        	if("button b1" == opac){
        		console.info(opac);
        		showConfirm.load("确定要撤销吗？",function(){
        			//进行撤销操作
                        $http.post('../order/makeOrderCancel.jspa', nu).success(function (json) {
                            var detail = eval("(" + json.detail + ")");
                            if (detail.code == 10000) {
                            	toastService.load('1',"撤销成功！！！");//撤销成功 提示框
                                $location.path("/myOrder");
                            } else {
                            	toastService.load('0',"撤销失败！！！");//撤销失败 提示框
                            }
                        });
        		});
        	}
        }
        //运输方式
    	var tranMode = [
    	            ['LRF','精准汽运（长途）'],
    	            ['FLF','精准卡航'],
    	            ['SRF','精准汽运（短途）'],
    	            ['FSF','精准城运'],
    	            ['PLF','汽运偏离'],
    	            ['AF','精准空运'],
    	            ['FV','整车'],
    	            ['PACKAGE','标准快递'],
    	            ['RCP','3.60特惠件'],
                    ['DTD','门到门'],
                    ['YTY','厂到厂']
    	        ];
        //送货方式
    	 var delMode = [
    	                ['DELIVER_NOUP','汽运送货（不含上楼）'],
    	                ['SELF_PICKUP','汽运自提'],
    	                ['DELIVER','汽运免费派送'],
    	                ['DELIVER_INGA','汽运送货进仓'],
    	                ['DELIVER_UP','汽运送货'],
    	                ['INNER_PICKUP','汽运内部带货自提'],
                        ['DELIVER_NOUP_AIR','空运送货（不含上楼）'],
                        ['SELF_PICKUP_AIR','空运自提（不含机场提货费）'],
                        ['SELF_PICKUP_FREE_AIR','空运免费自提'],
                        ['AIRPORT_PICKUP','空运机场自提'],
                        ['DELIVER_AIR','空运免费运货'],
                        ['DELIVER_INGA_AIR','空运送货进仓'],
                        ['DELIVER_UP_AIR','空运送货上楼']
    	                ];
        //代收货款
    	 var  reciveLoan = [
    	                    ['R1','即日退'],
    	                    ['R3','三日退'],
    	                    ['RA','审核退']
    	                ];
        //付款方式
    	 var  payMode = [
    	                 ['CH','现付'],
    	                 ['CD','银行卡'],
    	                 ['TT','电汇'],
    	                 ['NT','支票'],
                         ['OL','网上支付'],
                         ['CT','月结'],
                         ['DT','临时欠款'],
                         ['FC','到付']
    	             ];
        //签收单返回方式
    	 var  returnBillMode = [
    	                        ['NONE','无需返单'],
    	                        ['FAX','客户签收单传真返回'],
    	                        ['ORIGINAL','客户签收单原件返回'],
    	                        ['ARRIVESHEET_FAX','运单到达联传真返回']
    	                    ];
        $scope.isShow = true;//显示加载框
        $http.post('../order/queryOrderDetail.jspa',{"keyword":$scope.orderNumber}).success(function (json) {
            $scope.isShow = false;//隐藏加载框
            var detail = eval("(" + json.detail + ")");
            //发货人信息
            $scope.userFa =
            {
                userFa: detail.order.contactName,
                tel: detail.order.contactMobile,
                city: detail.order.contactCity,
                userAddress: detail.order.contactAddress,

                contactProvince: detail.order.contactProvince,
                contactCity: detail.order.contactCity,
                contactArea: detail.order.contactArea
            };
            //收货人信息
            $scope.userSou =
            {
                userSou: $scope.panduan(detail.order.receiverCustName),
                tel: $scope.panduan(detail.order.receiverCustMobile),
                city: $scope.panduan(detail.order.receiverCustCity),

                receiverCustProvince: $scope.panduan(detail.order.receiverCustProvince),
                receiverCustCity: $scope.panduan(detail.order.receiverCustCity),
                receiverCustArea: $scope.panduan(detail.order.receiverCustArea),
                userAddress: $scope.panduan(detail.order.receiverCustAddress)


            };

            //货物 信息
            $scope.huoWu =
            {
                hName: "",
                gNum: "",
                hKg: "",
                hV: "",
                baoJia: "",
                baoZhuan: ""
            };
        });

        $scope.backMyOrder = function (userFa, tel, userAddress, userSou, soutel, SouuserAddress) {
            if ($scope.userSou.receiverCustProvince == null || $scope.userSou.receiverCustProvince == "undefinded") {
                $scope.userSou.receiverCustProvince = "";
            }
            if ($scope.userSou.receiverCustCity == null || $scope.userSou.receiverCustCity == "undefinded") {
                $scope.userSou.receiverCustCity = "";
            }
            if ($scope.userSou.receiverCustArea == null || $scope.userSou.receiverCustArea == "undefinded") {
                $scope.userSou.receiverCustArea = "";
            }

            $scope.orderEntity = {

                shipperName: userFa,
                contactMobile: tel,
                contactCity: $scope.userFa.contactProvince + "-" + $scope.userFa.contactCity + "-" + $scope.userFa.contactArea,
                contactAddress: $scope.userFa.userAddress,

                receiverCustName: userSou,
                receiverCustMobile: soutel,
                receiverCustCity: $scope.userSou.receiverCustProvince + "-" + $scope.userSou.receiverCustCity + "-" + $scope.userSou.receiverCustArea,
                receiverCustAddress: $scope.userSou.userAddress,
                myVal: false,
                ionarrow: "ic_ion-arrow-up-b",
                myValBack: "1",
                orderNumber: $stateParams.id,
                orderStatus: $stateParams.status
            };
            if ($scope.orderEntity.receiverCustCity == "--" || $scope.orderEntity.receiverCustCity == "-") {
                $scope.orderEntity.receiverCustCity = "";
            }
            $rootScope.orderEntity = $scope.orderEntity;
            $location.path("/placeOrder");
            $rootScope.controlSystemBackButtonDetail = true;
        }

        $scope.check = function () {
            check($scope, $http, $location, $rootScope);
        }
        $scope.check2 = function () {
            check2($scope, $http, $location, $rootScope);
        }
        var waybillNumber = waybillNumberFac.getWaybill();
        $scope.waybill = null;
        if (waybillNumber !== null && waybillNumber !== "" && waybillNumber !== undefined) {
            $http.post('../WayBillNo/getWayBillNo.jspa',{"keyword":waybillNumber}).success(function (json) {
                if (json != null) {
                    var detail = eval("(" + json.detail + ")");
                    if (detail != null && detail != "" && detail.code == '10000') {
                        //发货人信息
                        $scope.userFa =
                        {
                            userFa: detail.detail.sender,
                            tel: detail.detail.senderMobile,
                            city: "",
                            userAddress: detail.detail.senderAddress,

                            contactProvince: "",
                            contactCity: "",
                            contactArea: ""
                        };
                        //收货人信息
                        $scope.userSou =
                        {
                            userSou: detail.detail.consignee,
                            tel: detail.detail.consigneeMobile,
                            city: "",

                            receiverCustProvince: "",
                            receiverCustCity: "",
                            receiverCustArea: "",
                            userAddress: detail.detail.consigneeAddress


                        };

                        //货物 信息
                        $scope.huoWu =
                        {
                            hName: detail.detail.goodName,
                            gNum: detail.detail.pieces,
                            hKg: detail.detail.weight,
                            hV: detail.detail.cubage,
                            baoJia: detail.detail.insuranceValue,
                            baoZhuan: detail.detail.packing
                        };
                        //服务信息
                        var tranPropNum=0;
                        for (var i = 0, len = tranMode.length; i < len; i++) {
                            if (tranMode[i][0] == detail.detail.tranProperty) {
                                $scope.tran = tranMode[i][1];
                                tranPropNum++;
                            }
                            if(tranPropNum==0){
                                $scope.tran="汽运偏离";
                            }
                        }
                        for (var i = 0, len = delMode.length; i < len; i++) {
                            if (delMode[i][0] == detail.detail.deliveryType) {
                                $scope.deliver = delMode[i][1];
                            }
                        }
                        for (var i = 0, len = returnBillMode.length; i < len; i++) {
                            if (returnBillMode[i][0] == detail.detail.signBillBackWay) {
                                $scope.returnBill = returnBillMode[i][1];
                            }
                        }
                        for (var i = 0, len = reciveLoan.length; i < len; i++) {
                            if (reciveLoan[i][0] == detail.detail.refundType) {
                                $scope.recive = reciveLoan[i][1];
                            }
                        }
                        for (var i = 0, len = payMode.length; i < len; i++) {
                            if (payMode[i][0] == detail.detail.payment) {
                                $scope.pay = payMode[i][1];
                            }
                        }
                    } else {
                        return;
                    }
                }
            })
        }
        $scope.$on('$destroy', function() {
            waybillNumberFac.setWaybill("");
        });
    })

