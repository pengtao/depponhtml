angular.module('starter.queryBranchesCtrl', []).controller('queryBranchesCtrl', function($scope, $document, $ionicModal, $http, $location, $ionicScrollDelegate, $rootScope, toastService) {
    var hasgeolocation = false;
    $scope.switchType = function(type) {
        if (type == 1) {
            if (switch2) {
                hasMore = true;
            }
            switch2 = false;
            document.querySelector("#mapContainer").style.display = "none";
            $scope.switch1 = 'dp-map-switch';
            $scope.switch2 = '';
            $scope.mapScroll = '';
            $scope.showBranches = true;
            $scope.showMap = false;
            if (window.navigator.geolocation) {
                var options = {
                    enableHighAccuracy: true,
                    timeout: 10000
                };
                if (!hasgeolocation) {
                    $scope.isShow = true;
                    window.navigator.geolocation.getCurrentPosition(function(position) {
                        hasgeolocation = true;
                        $scope.centerLongitude = position.coords.longitude;
                        $scope.centerLatitude = position.coords.latitude;
                        send();
                    }, function(error) {
                        $scope.centerLongitude = 121.110972;
                        $scope.centerLatitude = 31.139269;
                        switch (error.code) {
                            case error.PERMISSION_DENIED:
                                toastService.load('0', "定位失败，请设置开启定位服务");
                                break;
                            default:
                                toastService.load('0', "定位失败！请手动查询");
                                break;
                        }
                        send();
                    }, options);

                    function send() {
                        if (initial) {
                            initial = false;
                            var obj = {
                                "name": "",
                                "longitude": $scope.centerLongitude,
                                "latitude": $scope.centerLatitude,
                                "pageSize": 10,
                                "pageIndex": 0
                            };
                            $http.post('../branchInfo/branchList.jspa', obj).success(function(json) {
                                $scope.isShow = false;
                                var detail = json.detail;
                                $scope.branches = transDetail(detail);
                                hasMore = true;
                            });
                        }
                    }

                }
            } else {
                toastService.load('0', "您的手机不支持获取位置信息");
            }
        } else if (type == 2) {
            document.querySelector("#mapContainer").style.display = "";
            hasMore = false;
            switch2 = true;
            $scope.switch1 = '';
            $scope.switch2 = 'dp-map-switch';
            $scope.mapScroll = 'dp-map-scroll';
            $scope.showBranches = false;
            if ($scope.centerLongitude == undefined || $scope.centerLongitude == '') {
                if (window.navigator.geolocation) {
                    var options = {
                        enableHighAccuracy: true
                    };
                    window.navigator.geolocation.getCurrentPosition(function(position) {
                        $scope.centerLongitude = position.coords.longitude;
                        $scope.centerLatitude = position.coords.latitude;
                        $scope.initMap();
                    }, function(error) {}, options);
                } else {
                    toastService.load('0', "您的手机不支持获取位置信息");
                }
            } else {
                $scope.initMap();
            }
        }
    };
    $scope.getTelephones = function(tels) {
        var arrs = []
        if ( !! tels) {
            if (tels.indexOf("/") > -1) {
                arrs = tels.split('/');
                var arrl = arrs.length;
                for (var i = 1; i < arrl; i++) {
                    var l = arrs[i].length;
                    arrs[i] = arrs[i - 1].slice(0, -l) + "" + arrs[i];
                }
            } else {
                arrs.push(tels)
            }
            return arrs;
        }

    }

    /**
     * 初始显示列表，不显示地图
     */
    $scope.switchType(1);
    $scope.initMap = function() {
        $scope.mapOptions = {
            center: {
                longitude: $scope.centerLongitude,
                latitude: $scope.centerLatitude
            },
            zoom: 11,
            city: 'ShangHai',
            width: '100%',
            height: '240px'
        };
        if ($scope.cityName) {
            $scope.mapOptions.center = $scope.cityName;
        }
        //初始化地图上标记的点
        $scope.mapOptions.markers = [];
        for (var i = 0; i < $scope.branches.length; i++) {
            $scope.mapOptions.markers.push({
                longitude: $scope.branches[i].location_longitude,
                latitude: $scope.branches[i].location_latitude,
                icon: 'img/ic_map_marker.png',
                width: 20,
                height: 30,
                title: '',
                content: $scope.branches[i].name,
                name: $scope.branches[i].name,
                location_address: $scope.branches[i].location_address,
                business: $scope.branches[i].business,
                telephone: $scope.branches[i].oldtel,
                distance: $scope.branches[i].distance
            });
        }
        $scope.showMap = true;
        newMap();
    };
    $scope.goHere = function(location_longitude, location_latitude, name, location_address, business, telephone, distance) {
        hasMore = false;
        $ionicScrollDelegate.scrollTop();
        document.querySelector("#mapContainer").style.display = "";
        $scope.showMap = false;
        $scope.switch1 = '';
        $scope.switch2 = 'dp-map-switch';
        $scope.mapScroll = 'dp-map-scroll';
        switch2 = true;
        $scope.showBranches = false;
        telephone = $scope.getTelephones(telephone);
        var tels = telephone;
        $scope.mapBranches = [{
            'location_longitude': location_longitude,
            'location_latitude': location_latitude,
            'name': name,
            'location_address': location_address,
            'business': business,
            'telephone': telephone,
            'distance': distance
        }];
        $scope.mapOptions = {
            center: {
                longitude: $scope.centerLongitude,
                latitude: $scope.centerLatitude
            },
            zoom: 11,
            city: 'ShangHai',
            width: '100%',
            height: '240px',
            driverRoute: true,
            ori_local: [$scope.centerLongitude, $scope.centerLatitude],
            dest_local: [location_longitude, location_latitude]
        };
        //初始化地图上标记的点
        $scope.mapOptions.markers = [];
        $scope.showMap = true;
        newMap();
        //            $scope.routeMap(location_longitude,location_latitude);
        //            $scope.$apply();
    };

    $scope.routeMap = function(location_longitude, location_latitude, name, location_address, business, telephone, distance) {
        $ionicScrollDelegate.scrollTop();
        document.querySelector("#mapContainer").style.display = "";
        var map = new BMap.Map("mapContainer");
        document.querySelector("#mapContainer").style.width = $scope.mapOptions.width;
        document.querySelector("#mapContainer").style.height = $scope.mapOptions.height + 'px';
        // init map, set central location and zoom level
        map.centerAndZoom(new BMap.Point($scope.centerLongitude, $scope.centerLatitude), $scope.mapOptions.zoom);

        var p1 = new BMap.Point($scope.centerLongitude, $scope.centerLatitude);
        var p2 = new BMap.Point(location_longitude, location_latitude);
        var driving = new BMap.DrivingRoute(map, {
            renderOptions: {
                map: map,
                autoViewport: true
            }
        });
        driving.search(p1, p2);
        switch2 = true;
        // add navigation control
        map.addControl(new BMap.NavigationControl());
        // add scale control
        map.addControl(new BMap.ScaleControl());

        //enable scroll wheel zoom
        map.enableScrollWheelZoom();

        // set the city name
        map.setCurrentCity($scope.mapOptions.city);

    };
    $scope.testClick = function(longitude, latitude, name, location_address, business, telephone, distance) {
        telephone = $scope.getTelephones(telephone);
        $scope.mapBranches = [{
            'location_longitude': longitude,
            'location_latitude': latitude,
            'name': name,
            'location_address': location_address,
            'business': business,
            'telephone': telephone,
            'distance': distance
        }];
        $scope.$apply();
    };
    var pageIndex = 0
    var hasMore = false,
        initial = true;
    var Bbranches = [],
        //是否是地图模式
        switch2 = false;

    $scope.queryBranches = function(name, cityCode, sendcity, pageidx) {
        name = name ? name : "";
        var cCodes = [],
            citys = [],
            province_code, city_code, area_code; !! cityCode ? cCodes = cityCode.split("^") : ""; !! sendcity ? citys = sendcity.split("-") : "";
        if (cCodes.length == 3) {
            province_code = cCodes[0];
            city_code = cCodes[1];
            area_code = cCodes[2];
        } else if (cCodes.length == 2) {
            province_code = cCodes[0];
            city_code = cCodes[1];
        } else {
            province_code = cCodes[0];
        }
        if (citys.length > 0) {
            $scope.cityName = citys[citys.length - 1];
        }
        if (pageidx === 0) {
            pageIndex = pageidx;
            Bbranches = []
        }
        //TODO
        //       	 var obj = {'province_code':province_code,'city_code':city_code,'area_code':area_code,"name": name, "longitude":121.33017783194, "latitude": 31.251269499009,"pageSize":10,"pageIndex":++pageIndex};
        var obj = {
            'province_code': province_code,
            'city_code': city_code,
            'area_code': area_code,
            "keyWords": name,
            "longitude": $scope.centerLongitude,
            "latitude": $scope.centerLatitude,
            "pageSize": 10,
            "pageIndex": pageIndex++
        };
        hasMore = true;
        $scope.isShow = true;
        $http.post('../branchInfo/branchList.jspa', obj).success(function(json) {
            $scope.isShow = false;
            var detail = json.detail;;
            hasMore = true;
            if (detail && detail.length > 0) {
                detail = transDetail(detail);
                for (var x = 0; x < detail.length; x++) {
                    Bbranches.push(detail[x]);
                }

                if (detail.length == 10) {
                    $scope.loadMoreWord = "上拉查看更多";
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    hasMore = false;
                    $scope.loadMoreWord = "";
                }

            }
            $scope.branches = Bbranches;
            if ($scope.branches.length == 0) {
                $scope.loadMoreWord = "";
                toastService.load('0', "亲，该地区暂未开通相关业务，换个地址试一下吧");
            }
            $scope.showMap = false;
            if (switch2) {
                $scope.initMap();
            }


        });
    };
    $scope.hasMoreData = function() {
        return hasMore;
    }
    business = 3;
    citys($scope, $http, $location, $ionicModal, $ionicScrollDelegate, $rootScope);
    $scope.check = function() {
        check($scope, $http, $location, $rootScope);
    }
    $scope.check2 = function() {
        check2($scope, $http, $location, $rootScope);
    }
    $scope.inFocus = function(e) {
        var target = e.target;
        target.blur();
    };

    function transDetail(detail) {
        for (var i = 0; i < detail.length; i++) {
            var business = detail[i].business
            var dist = detail[i].distance / 1000;
            if (dist > 1) {
                detail[i].distance = Math.ceil(dist) + "公里";
            } else {
                detail[i].distance = detail[i].distance + "米";
            }
            if (business == "11111") {
                detail[i].business = "可发货 可自提 可派送"
            } else if (business == "01111") {
                detail[i].business = "可自提 可派送 "
            } else if (business == "10111") {
                detail[i].business = "可发货 可派送"
            } else if (business == "11011") {
                detail[i].business = "可发货 可自提"
            } else if (business == "11101") {
                detail[i].business = "可发货 可自提 可派送 "
            } else if (business == "11110") {
                detail[i].business = "可发货 可自提 可派送 "
            } else if (business == "00111") {
                detail[i].business = "可派送 "
            } else if (business == "01011") {
                detail[i].business = "可自提 "
            } else if (business == "01101") {
                detail[i].business = "可自提 可派送 "
            } else if (business == "01110") {
                detail[i].business = "可自提 可派送 "
            } else if (business == "10011") {
                detail[i].business = "可发货 "
            } else if (business == "10101") {
                detail[i].business = "可发货 可派送 "
            } else if (business == "10110") {
                detail[i].business = "可发货 可派送"
            } else if (business == "11001") {
                detail[i].business = "可发货 可自提"
            } else if (business == "11010") {
                detail[i].business = "可发货 可自提 "
            } else if (business == "11100") {
                detail[i].business = "可发货 可自提 可派送"
            } else if (business == "00011") {
                detail[i].business = ""
            } else if (business == "00101") {
                detail[i].business = "可派送 "
            } else if (business == "00110") {
                detail[i].business = " 可派送 "
            } else if (business == "01001") {
                detail[i].business = "可自提 "
            } else if (business == "01010") {
                detail[i].business = "可自提"
            } else if (business == "01100") {
                detail[i].business = "可自提 可派送"
            } else if (business == "00001") {
                detail[i].business = ""
            } else if (business == "00010") {
                detail[i].business = ""
            } else if (business == "00100") {
                detail[i].business = "可派送"
            } else if (business == "01000") {
                detail[i].business = "可自提"
            } else if (business == "10001") {
                detail[i].business = "可发货 "
            } else if (business == "10010") {
                detail[i].business = "可发货 "
            } else if (business == "10100") {
                detail[i].business = "可发货 可派送"
            } else if (business == "11000") {
                detail[i].business = "可发货 可自提"
            } else if (business == "10000") {
                detail[i].business = "可发货"
            } else if (business == "00000") {
                detail[i].business = ""
            }
            detail[i].oldtel = detail[i].telephone;
            detail[i].telephone = $scope.getTelephones(detail[i].telephone);
        }
        return detail;

    }

    function drawInfo(opts) {
        if (!opts || !opts.map) {
            return;
        }
        var map = opts.map,
            h = opts.height || 20,
            w = opts.width || 100,
            offset = opts.offset || {
                top: 30,
                left: 0
            };

        // 定义自定义覆盖物的构造函数  
        var SquareOverlay = function(center, w) {
            this._center = center;
            this._width = w;
            this._div;
        }
        // 继承API的BMap.Overlay    
        SquareOverlay.prototype = new BMap.Overlay();
        // 实现初始化方法  
        SquareOverlay.prototype.initialize = function(map) {
            // 保存map对象实例   
            this._map = map;
            // 创建div元素，作为自定义覆盖物的容器   
            var div = document.createElement("div");
            //                          div.className="pop";
            div.style.position = "absolute";
            // 可以根据参数设置元素外观   
            div.style.width = this._width + "px";
            div.style.textAlign = 'center';
            div.style.minHeight = this._height + "px";
            //                          div.style.background = this._color;
            // 将div添加到覆盖物容器中   
            map.getPanes().markerPane.appendChild(div);
            // 保存div实例   
            this._div = div;
            // 需要将div元素作为方法的返回值，当调用该覆盖物的show、   
            // hide方法，或者对覆盖物进行移除时，API都将操作此元素。   
            return div;
        }
        SquareOverlay.prototype.setCenter = function(position) {
            // this._center = center;
            this._div.style.left = position.x - this._width / 2 - offset.left + "px";
            this._div.style.top = position.y - offset.top + "px";

        }
        //给信息弹框设置样式
        SquareOverlay.prototype.set_Css = function(css) {
            for (var name in css) {
                this._div.style[name] = css[name];
            }
        }
        //  给信息弹框设置innerHTML
        SquareOverlay.prototype.setContent = function(content) {
            this._div.innerHTML = content;
            //this._div.innerHTML = '<div class="pop">上海市青浦城区营业部>>  </div>';
        }
        // 实现绘制方法   
        SquareOverlay.prototype.draw = function() {
            // 根据地理坐标转换为像素坐标，并设置给容器    
            var position = this._map.pointToOverlayPixel(this._center);
            this._div.style.left = position.x - this._width / 2 - offset.left + "px";
            this._div.style.top = position.y - offset.top + "px";
            //this._div.innerHTML = '<div class="pop">上海市青浦城区营业部>>  </div>';
        }
        // 实现显示方法    
        SquareOverlay.prototype.show = function() {
            if (this._div) {
                this._div.style.display = "";
            }
        }
        // 实现隐藏方法  
        SquareOverlay.prototype.hide = function() {
            if (this._div) {
                this._div.style.display = "none";
            }
        }
        SquareOverlay.prototype.toggle = function() {
            if (this._div) {
                if (this._div.style.display == "") {
                    this.hide();
                } else {
                    this.show();
                }
            }
        }

        var mySquare = new SquareOverlay(map.getCenter(), w);
        return mySquare;

    }

    function newMap() {
        var defaults = {
            navCtrl: true,
            scaleCtrl: true,
            overviewCtrl: true,
            enableScrollWheelZoom: true,
            zoom: 10,
            driverRoute: false
        };
        var element = document.querySelector("#mapContainer");
        var checkNull = function(obj) {
            return obj === null || obj === undefined;
        };

        var checkMandatory = function(prop, desc) {
            if (!prop) {
                throw new Error(desc);
            }
        };
        var ops = {}, hasCityName = false,
            infoWind;
        ops.navCtrl = checkNull($scope.mapOptions.navCtrl) ? defaults.navCtrl : $scope.mapOptions.navCtrl;
        ops.scaleCtrl = checkNull($scope.mapOptions.scaleCtrl) ? defaults.scaleCtrl : $scope.mapOptions.scaleCtrl;
        ops.overviewCtrl = checkNull($scope.mapOptions.overviewCtrl) ? defaults.overviewCtrl : $scope.mapOptions.overviewCtrl;
        ops.enableScrollWheelZoom = checkNull($scope.mapOptions.enableScrollWheelZoom) ? defaults.enableScrollWheelZoom : $scope.mapOptions.enableScrollWheelZoom;
        ops.zoom = checkNull($scope.mapOptions.zoom) ? defaults.zoom : $scope.mapOptions.zoom;
        ops.driverRoute = checkNull($scope.mapOptions.driverRoute) ? defaults.driverRoute : $scope.mapOptions.driverRoute;
        checkMandatory($scope.mapOptions.width, 'options.width must be set');
        checkMandatory($scope.mapOptions.height, 'options.height must be set');
        checkMandatory($scope.mapOptions.center, 'options.center must be set');
        if (typeof $scope.mapOptions.center == "string") {
            hasCityName = true;
        } else {
            checkMandatory($scope.mapOptions.center.longitude, 'options.center.longitude must be set');
            checkMandatory($scope.mapOptions.center.latitude, 'options.center.latitude must be set');

        }
        checkMandatory($scope.mapOptions.city, 'options.city must be set');

        ops.width = $scope.mapOptions.width;
        ops.height = $scope.mapOptions.height;
        if (!hasCityName) {
            ops.center = {
                longitude: $scope.mapOptions.center.longitude,
                latitude: $scope.mapOptions.center.latitude
            };
        }
        ops.city = $scope.mapOptions.city;
        ops.markers = $scope.mapOptions.markers;

        element.style.width = ops.width;
        element.style.height = ops.height;

        // create map instance
        var map = new BMap.Map("mapContainer");

        // init map, set central location and zoom level
        if (!hasCityName) {
            map.centerAndZoom(new BMap.Point(ops.center.longitude, ops.center.latitude), ops.zoom);
        } else {
            map.centerAndZoom($scope.mapOptions.center);
        }
        if (ops.driverRoute) {
            var p1 = new BMap.Point($scope.mapOptions.ori_local[0], $scope.mapOptions.ori_local[1]);
            var p2 = new BMap.Point($scope.mapOptions.dest_local[0], $scope.mapOptions.dest_local[1]);
            var driving = new BMap.DrivingRoute(map, {
                renderOptions: {
                    map: map,
                    autoViewport: true
                }
            });
            driving.search(p1, p2);
        }
        if (ops.navCtrl) {
            // add navigation control
            map.addControl(new BMap.NavigationControl());
        }
        if (ops.scaleCtrl) {
            // add scale control
            map.addControl(new BMap.ScaleControl());
        }
        if (ops.overviewCtrl) {
            //add overview map control
            map.addControl(new BMap.OverviewMapControl());
        }
        if (ops.enableScrollWheelZoom) {
            //enable scroll wheel zoom
            map.enableScrollWheelZoom();
        }

        // set the city name
        map.setCurrentCity(ops.city);

        if (!ops.markers) {
            return;
        }
        //create markers
        map.addEventListener("click", function() {
            infoWind && infoWind.hide();
        });
        // map.addOverlay(new BMap.Marker(new BMap.Point($scope.centerLongitude, $scope.centerLatitude), {
        //              icon: new BMap.Icon("img/ic_branch_addr.png", new BMap.Size(30, 30))
        //          }));
        var openInfoWindow = function(marker) {
            return function(e) {
                // console.log("点击事件" + e);
                e.domEvent.stopPropagation();
                var opts = {
                    map: map,
                    width: 320
                }
                if (!infoWind) {
                    infoWind = drawInfo(opts);
                    map.addOverlay(infoWind);
                }
                map.setCenter(new BMap.Point(marker.longitude, marker.latitude));
                var position = map.pointToOverlayPixel(new BMap.Point(marker.longitude, marker.latitude));
                infoWind.addEventListener("click", function(e) {
                    e.domEvent.stopPropagation();
                });
                infoWind.setCenter(position);
                infoWind.hide();
                infoWind.setContent('<div class="pop">' + marker.name + '</div>');
                infoWind.show();
                $ionicScrollDelegate.scrollTo(0, 220);
                //                document.querySelector(".scroll").style.webkitTransform = 'translate3d(0px, -215px, 0px) scale(1)';
                // this.openInfoWindow(infoWin);
                $scope.testClick(marker.longitude, marker.latitude, marker.name, marker.location_address, marker.business, marker.telephone, marker.distance);
            };
        };

        for (var i in ops.markers) {
            var marker = ops.markers[i];
            var pt = new BMap.Point(marker.longitude, marker.latitude);
            var marker2;
            if (marker.icon) {
                var icon = new BMap.Icon(marker.icon, new BMap.Size(marker.width, marker.height));
                marker2 = new BMap.Marker(pt, {
                    icon: icon
                });
            } else {
                marker2 = new BMap.Marker(pt);
            }
            // add marker to the map
            map.addOverlay(marker2); // 将标注添加到地图中

            if (!marker.title && !marker.content) {
                return;
            }
            var infoWindow2 = new BMap.InfoWindow("<p>" + (marker.title ? marker.title : '') + "</p><p>" + (marker.content ? marker.content : '') + "</p>", {
                enableMessage: checkNull(marker.enableMessage) ? false : marker.enableMessage
            });
            marker2.addEventListener("click", openInfoWindow(marker));
        }
    }

})