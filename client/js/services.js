angular.module('starter.services', [])
/**
 * 加载框组件
 */
    .directive('loadCompent', function () {
        return {
            restrict: 'EA',
            compile: function (tElement, tAttrs) {
                tElement
                    .append(
                        '<div ng-show="' + tAttrs.loadCompent + '" class="backdrop" style="visibility:visible;opacity:1">' +
                        '<div class="loading-container visible active" style="opacity:1;">' +
                        '<div class="loading" style="opacity: 1;">' +
                        '<div style="border-radius:5px;text-align:center;">' +
                        '<img src="img/loading.gif" class="load-img">' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                );
            }
        };
    })
/**
 * 提示框组件
 */
    .service('toastService', function ($ionicLoading, $timeout) {
        return {
            load: function (state, title) {
                var srcTest;
                if (state == '0') {
                    srcTest = "img/toast_cry.png";
                } else if (state == '1') {
                    srcTest = "img/toast_smile.png";
                }
                $ionicLoading.show({
                    template: '<div style="background:#FFFFFF;padding: 10px;text-align: center;border-radius: 10px;opacity: 0.9;width: 160px">' +
                        '<img src=' + srcTest + ' style="width:50%;margin-bottom: 10px;margin-top:15px">' +
                        '<p style="color: #171723;font-size: 12px;">' + title +
                        '</p>' +
                        '</div>'
                });
                $timeout(function () {
                    $ionicLoading.hide();
                }, 2000);
            }
        };
    })
/**
 * confirm重写
 */
    .service('showConfirm', function ($ionicPopup) {
        return {
            load: function (message, func) {
                var confirmPopup = $ionicPopup.confirm({
                    title: message,
//                    template: 'Are you sure you want to eat this ice cream?'
                    cancelText: '确定',
                    cancelType: 'button-positive',
                    okText: '取消',
                    okType: 'button-default'
                });
                confirmPopup.then(function (res) {
                    if (res) {//确定
                        console.log('You are sure');

                    } else {//取消
                        func();
                        console.log('You are not sure');
                    }
                });
            }
        };
    })

    .directive('dpNavHref', function () {
        return {
            restrict: 'A',
            compile: function (tElement, tAttrs) {
                tElement
                    .append(
                        '<div dp-list-open class="bar bar-header dp-bar item-icon-right" style="padding:0px;font-size:14px;">' +
                        '<div style="width:30%;height:43px;"><a class="dp-icon-back" href="' + tAttrs.dpNavHref + '">' +
                        '<img src="img/back.png" style="height:50%;margin-top:11px;"/>'
                        + '</a></div>' +
                        '<h1 class="title" style="width:40%;text-align:center;position:relative;margin:0px;line-height:43px;">' + tAttrs.dpNavTitle + '</h1>' +
                        '<div style="width:30%;"><a href="#" class="" style="height:43px;width:25px;">' +
                        '<i class="icon" style="right:0px;">' +
                        '<img src="img/index.png" style="height:50%;right: 50px;top:11px;position:absolute;">' +
                        '</i>' +
                        '</a>' +
                        '<img class="icon" src="img/ic_list.png" style="height:45%;top:13px;float:right;" ng-click="openList(switch=(switch==false)?true:false)"/></div>'
                        + '</div>'
                );
            }
        };
    })

    .directive('dpBackNavHref', function () {

        return {
            restrict: 'A',
            compile: function (tElement, tAttrs) {
                tElement
                    .append(
                        '<div dp-list-open class="bar bar-header dp-bar item-icon-right" style="padding:0px;font-size:14px;">' +
                        '<div style="width:30%;height:43px;"><a class="dp-icon-back" ng-click="' + tAttrs.dpBackNavHref + '">' +
                        '<img src="img/back.png" style="height:50%;margin-top:11px;">'
                        + '</a></div>' +
                        '<h1 class="title" style="width:40%;text-align:center;position:relative;margin:0px;line-height:43px;">' + tAttrs.dpNavTitle + '</h1>' +
                        '<div style="width:30%;"><a class="" style="height:43px;width:25px;" ng-click="goHome()">' +
                        '<i class="icon" style="right: 0px;">' +
                        '<img src="img/index.png" style="height:50%;right: 50px;top:11px;position:absolute;">' +
                        '</i>' +
                        '</a>' +
                        '<img class="icon" src="img/ic_list.png" style="height:45%;top:13px;float:right;" ng-click="openList(switch=(switch==false)?true:false)"/></div>'
                        + '</div>'
                );
            }
        };
    })
    .directive('dpContentFooter', function () {
        return {
            restrict: 'A',
            compile: function (tElement, tAttrs) {
                tElement
                    .append(
                        '<div  class="dp-footer-style" style="width:100%;">' +
                        '<div class="dp-footer">' +
                        '<div style="font-size:14px;"><a href="tel:95353">' +
                        '<img src="img/phone.png" style="width:22%;height:22%;"> <span class="hotline" style="font-size:0.846em;">(服务热线)</span>' +
                        '</a></div>' +
                        '<div style="font-size:0.9em;">' +
                        '<a href="#/home" style="color:#338FED">触屏版</a>|<a href="http://www.deppon.com/?bdmark=pc" style="color:#6D6D6D">电脑版</a>|' +
                        '<a href="https://dpapp.deppon.com/app/qr/index.html" style="color:#6D6D6D">德邦APP</a>' +
                        '</div>' +
                        '<div style="font-size:14px;">' +
                        '<p class="hotline" style="fon-size:0.846em;"> © 2015 德邦物流股份有限公司.沪ICP备10005645</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                );
            }
        }
    })

    .directive('dpListOpen', function () {
        return {
            restrict: 'A',
            compile: function (tElement, tAttrs) {
                tElement
                    .append(
                        '<div ng-init="switch=false" ng-show="switch" >' +
                        '<div class="dp-home-menu2" style="z-index:999">' +
                        '<div class="row">' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" ng-click="check()"> <img ' +
                        'src="img/mydeppon.png">' +

                        '<p>我的德邦</p>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" ng-click="check2()"> <img ' +
                        'src="img/xiadan.png">' +

                        '<p>我要下单</p>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" href="#/goodSelect"> <img ' +
                        'src="img/hw_genzong.png">' +

                        '<p>货物追踪</p>' +
                        '</a>' +
                        '</div>' +
                        '</div>' +

                        '<div class="row">' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" href="#/queryBranches"> <img ' +
                        'src="img/wd_chaxun.png">' +

                        '<p>网点查询</p>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" href="#/realTimePrice"> <img ' +
                        'src="img/jiage.png">' +

                        '<p>价格时效</p>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" href="#/expressDispatch"> <img ' +
                        'src="img/fanwei.png">' +

                        '	<p>快递收送范围</p>' +
                        '</a>' +
                        '</div>' +
                        '</div>' +
                        '<div class="row" ng-click="openList(switch=(switch==false)?true:false)" style="width: 100%; min-height: 100%;height: 3000px;background: #000;opacity: 0.6;"></div>' +
                        ' </div>' +
                        ' </div>'
                )
            }
        }
    })
    .directive('dpNavModal', function () {

        return {
            restrict: 'A',
            compile: function (tElement, tAttrs) {
                tElement
                    .append(
                        '<div dp-list-modal-open class="bar bar-header dp-bar item-icon-right" style="padding:0px;">' +
                        '<div style="width:25%;height:43px;"><a class="dp-icon-back" ng-click="' + tAttrs.dpNavModal + '">' +
                        '<img src="img/back.png" style="height:50%;margin-top:11px;"/>'
                        + '</a></div>' +
                        '<h1 class="title" style="width:50%;text-align:center;position:relative;margin:0px;line-height:43px;">' + tAttrs.dpNavTitle + '</h1>' +
                        '<div style="width:25%;"><a class="" style="height:43px;width:25px;" ng-click="goHome()">' +
                        '<i class="icon" style="right:0px;">' +
                        '<img src="img/index.png" style="height:50%;right: 50px;top:11px;position:absolute;">' +
                        '</i>' +
                        '</a>' +
                        '<img class="icon" src="img/ic_list.png" style="height:45%;top:13px;float:right;" ng-click="openList(switch=(switch==false)?true:false)"/></div>'
                        + '</div>'
                );
            }
        };
    })
    .directive('dpListModalOpen', function () {
        return {
            restrict: 'A',
            compile: function (tElement, tAttrs) {
                tElement
                    .append(
                        '<div ng-init="switch=false" ng-show="switch">' +
                        '<div class="dp-home-menu2">' +
                        '<div class="row">' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" ng-click="check()"> <img ' +
                        'src="img/mydeppon.png">' +

                        '<p>我的德邦</p>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" ng-click="check2()"> <img ' +
                        'src="img/xiadan.png">' +

                        '<p>我要下单</p>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" ng-click="check3()"> <img ' +
                        'src="img/hw_genzong.png">' +

                        '<p>货物追踪</p>' +
                        '</a>' +
                        '</div>' +
                        '</div>' +

                        '<div class="row">' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" ng-click="check4()"> <img ' +
                        'src="img/wd_chaxun.png">' +

                        '<p>网点查询</p>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" ng-click="check5()"> <img ' +
                        'src="img/jiage.png">' +

                        '<p>价格时效</p>' +
                        '</a>' +
                        '</div>' +
                        '<div class="col" align="center">' +
                        '<a class="item item-thumbnail-top" ng-click="check6()"> <img ' +
                        'src="img/fanwei.png">' +

                        '	<p>快递收送范围</p>' +
                        '</a>' +
                        '</div>' +
                        '</div>' +
                        '<div class="row" ng-click="openList(switch=(switch==false)?true:false)" style="width: 100%; min-height: 100%;height: 3000px;background: #000;opacity: 0.6;"></div>' +
                        ' </div>' +
                        ' </div>' +
                        ' </div>'
                );
            }
        }
    })
    //注册成功用户名填充
    .factory('userNameFac', function () {
        var myUserName;
        return {
            getCurrentUser: function () {
                return myUserName;
            },
            setCurrentUser: function (userName) {
                myUserName = userName;
            }
        };
    })
    //运单号的获得
    .factory('waybillNumberFac', function () {
        var myWaybillNumber;
        return {
            getWaybill: function () {
                return myWaybillNumber;
            },
            setWaybill: function (waybillNumber) {
                myWaybillNumber = waybillNumber;
            }
        };
    })
    
    .factory('personInfo',function(){
    	var personInfo;
    	return{
    		getPersonInfo: function(){
    			return personInfo;
    		},
    	    setPersonInfo: function(e){
    	    	personInfo = e;
    	    }
    	};
    })
;
    