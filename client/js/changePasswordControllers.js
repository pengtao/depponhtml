angular.module('starter.changePasswordControllers', []) 
//密码修改
.controller('changePasswordCtrl', function ($scope,$http,$timeout,$location,$rootScope,$ionicLoading,toastService) {
	$scope.isShow=true;//显示加载框
	$http.post('../login/getSessionInfo.jspa').success(function (json) {
		$scope.isShow=false;//显示加载框
    	var user = json.userRest;
    	if(user==null){
			$location.path("/login/myDeppon");
		}
	$scope.errorText="";
	$scope.checkOldPwd = function(){
        if($scope.oldPwd==""|$scope.oldPwd===undefined|!$scope.changePassword.oldPwd.$valid){
            $scope.errorText="提示：密码必须为6到16位，请勿包含字母和数字以外的字符！";
        }else{
            $scope.errorText="";
        }
    }
	
	$scope.checkNewPwd = function(){
        if($scope.newPwd==""|$scope.newPwd===undefined|!$scope.changePassword.newPwd.$valid){
            $scope.errorText="提示：密码必须为6到16位，请勿包含字母和数字以外的字符！";
        }else if($scope.newPwd==$scope.oldPwd){
            $scope.errorText="提示：新密码与原密码相同，请重设！";
        }else{
            $scope.errorText="";
        }
    }
	
	$scope.checkCfgPassword = function(){
        if($scope.newPwd==""|$scope.newPwd===undefined|!$scope.changePassword.cfgPassword.$valid){
            $scope.errorText="提示：密码必须为6到16位，请勿包含字母和数字以外的字符！";
        }else if($scope.newPwd!=$scope.cfgPassword){
            $scope.errorText="提示：两次输入的新密码不一致，请核实！";
        }
        else{
            $scope.errorText=""; 
        }
    }
	
	$scope.checklc = function () {
        
        if($scope.oldPwd==null|$scope.newPwd==null|$scope.cfgPassword==null) {
            $scope.errorText="提示：密码必须为6到16位，请勿包含字母和数字以外的字符！";
            $timeout(function () {
        		$scope.errorText="";
            }, 5000);
        }else if($scope.newPwd!=$scope.cfgPassword){
        	$scope.errorText="提示：两次输入的新密码不一致，请核实！";
        	$timeout(function () {
        		$scope.errorText="";
            }, 5000);
        }else if($scope.newPwd==$scope.oldPwd)
        	{
        	$scope.errorText="提示：新密码与原密码相同，请重设！";
        	$timeout(function () {
        		$scope.errorText="";
            }, 5000);
        	}
        else{
            if ($scope.changePassword.$valid) {
            var obj={"userName":user.userName,"oldPwd":baseMEncode($scope.oldPwd),"newPwd":baseMEncode($scope.newPwd)};
            $scope.isShow=true;//显示加载框
            $http.post('../user/editPassword.jspa',obj).success(function (json) {
            	$scope.isShow=false;//隐藏加载框
            	var detail = eval("(" + json.detail + ")");
                if (detail.code == '10000') {
                	
                	toastService.load('1',"恭喜您修改密码成功");//显示提示框
                	$location.path("/login/myDoppon");
//                	$ionicLoading.show({
//                        template: '<div style="background:#ffffff;padding:15px;text-align: center;"><img src="img/order_success.png" style="width:35%;margin-bottom: 10px"/><p style="color:#171723;font-size: 15px;font-weight: bold">恭喜您修改密码成功!</p></div>'
//                    });
//                    $timeout(function() {
//                        $ionicLoading.hide();
//                        $location.path("/login/myDoppon");
//                    }, 2000);
                	
                } else if(detail.code == '20013') {
                    $scope.errorText = "提示：原密码为空";
                }else if(detail.code == '20014'){
                	$scope.errorText = "提示：原密码错误";
                }else if(detail.code == '20005'){
                	$scope.errorText = "提示：新密码为空";
                }
                else{
                	toastService.load('0',"修改密码失败，稍后重试");//显示提示框
                }
//                    $timeout(function () {
//                        $scope.errorText = "";
//                    }, 5000);
            });
       }
     }
        
	}
	
	}).error(function () {
        $location.path("/login/myDeppon");
    });
    $scope.check=function(){  check($scope,$http,$location,$rootScope);}
    $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
	
})
;
