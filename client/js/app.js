angular.module('starter',
    [ 'ionic','oc.lazyLoad','starter.services'])
    .run(function ($rootScope, $location) {
        $rootScope.$on('$locationChangeStart',function(evt, absNewUrl, absOldUrl) {
            $rootScope.history = absNewUrl.split('#')[1];
        });
    })
    
    //'oc.lazyLoad'启动延迟加载服务
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: "/home",
                views: {
                    "lazyLoadView": {
                        controller: "homeCtrl",
                        templateUrl: function () {
                            return  "templates/home.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.controllers',
                            serie: true,
                            files: ['js/controllers.js']
                        });
                    }]
                }
            })
            .state('productList', {
                url: "/productList",
                views: {
                    "lazyLoadView": {
                        templateUrl: function () {
                            return  "templates/productList.html";
                        }
                    }
                }
            })
            .state('marketList', {
                url: "/marketList",
                views: {
                    "lazyLoadView": {
                        templateUrl: function () {
                            return  "templates/marketList.html";
                        }
                    }
                }
            })
            .state('chengyun', {
                url: "/chengyun",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/productDetail/chengyun.html";
                        }
                    }
                }
            })
            .state('kahang', {
                url: "/kahang",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/productDetail/kahang.html";
                        }
                    }
                }
            })
            .state('qiyun', {
                url: "/qiyun",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/productDetail/qiyun.html";
                        }
                    }
                }
            })

            .state('kongyun', {
                url: "/kongyun",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/productDetail/kongyun.html";
                        }
                    }
                }
            })
            .state('360', {
                url: "/360",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/productDetail/360.html";
                        }
                    }
                }
            })
            .state('biaozhun', {
                url: "/biaozhun",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/productDetail/biaozhun.html";
                        }
                    }
                }
            })
            .state('biaozhun2', {
                url: "/biaozhun2",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/marketActivityDetail/biaozhun2.html";
                        }
                    }
                }
            })
            .state('zhengche', {
                url: "/zhengche",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/productDetail/zhengche.html";
                        }
                    }
                }
            })
            .state('deppon18', {
                url: "/deppon18",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/marketActivityDetail/deppon18.html";
                        }
                    }
                }
            })
            .state('lineDiscount', {
                url: "/lineDiscount",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/marketActivityDetail/lineDiscount.html";
                        }
                    }
                }
            })
            .state('precisionHighTicket', {
                url: "/precisionHighTicket",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/marketActivityDetail/precisionHighTicket.html";
                        }
                    }
                }
            })
            /* 找回密码 */
            .state('findBackPassword', {
                url: "/findBackPassword",
                views: {
                    "lazyLoadView": {
                        controller: "findBackPasswordCtrl",
                        templateUrl: function () {
                            return  "templates/loginAndRegister/findBackPassword.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.findBackPasswordControllers',
                            files: ['js/findBackPasswordControllers.js']
                        });
                    }]
                }
            })
            /* 我的德邦 页面 */
            .state('myDeppon', {
                url: "/myDeppon",
                views: {
                    "lazyLoadView": {
                        controller: "myDepponCtrl",
                        templateUrl: function () {
                            return  "templates/myDeppon.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.depponAndPersonControllers',
                            files: ['js/depponAndPersonControllers.js']
                        });
                    }]
                }
            })

            /* 密码修改 */
            .state('changePassword', {
                url: "/changePassword",
                views: {
                    "lazyLoadView": {
                        controller: "changePasswordCtrl",
                        templateUrl: function () {
                            return  "templates/changePassword.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.changePasswordControllers',
                            files: ['js/changePasswordControllers.js']
                        });
                    }]
                }
            })

            /* 个人资料 */
            .state('personInformation', {
                url: "/personInformation",
                views: {
                    "lazyLoadView": {
                        controller: "personInformationCtrl",
                        templateUrl: function () {
                            return  "templates/personInformation.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.depponAndPersonControllers',
                            files: ['js/depponAndPersonControllers.js']
                        });
                    }]
                }
            })

            /* 绑定手机号 */
            .state('bindPhone', {
                url: "/bindPhone",
                views: {
                    "lazyLoadView": {
                        controller: "bindPhoneCtrl",
                        templateUrl: function () {
                            return  "templates/bindPhone.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.bindPhoneControllers',
                            files: ['js/bindPhoneControllers.js']
                        });
                    }]
                }
            })

        /**
         * 选择城市测试
         */
            .state('queryBranches', {
                url: "/queryBranches",
                views: {
                    "lazyLoadView": {
                        controller: "queryBranchesCtrl",
                        templateUrl: function () {
                            return  "templates/query/queryBranches.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.queryBranchesCtrl',
                            files: ['js/queryBranchesCtrl.js'
                                     ]
                        });
                    }]
                }
            })
            .state('queryBranchesMap', {
                url: "/queryBranchesMap",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/marketActivityDetail/queryBranchesMap.html";
                        }
                    }
                }
            })

            .state('expressDispatch', {
                url: "/expressDispatch",
                views: {
                    "lazyLoadView": {
                        controller: "expressDispatchCtrl",
                        templateUrl: function () {
                            return  "templates/query/expressDispatch.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.queryControllers',
                            files: ['js/queryControllers.js']
                        });
                    }]
                }
            })
            .state('login', {
                url: "/login/:id",
                views: {
                    "lazyLoadView": {
                        controller: "LoginCtrl",
                        templateUrl: function () {
                            return  "templates/loginAndRegister/login.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.loginControllers',
                            files: ['js/loginControllers.js']
                        });
                    }]
                }
            })

            .state('register', {
                url: "/register",
                views: {
                    "lazyLoadView": {
                        controller: "RegisterCtrl",
                        templateUrl: function () {
                            return  "templates/loginAndRegister/register.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.loginControllers',
                            files: ['js/loginControllers.js']
                        });
                    }]
                }
            })

            /*价格时效查询*/

        .state('realTimePrice', {
            url: "/realTimePrice",
            views: {
                "lazyLoadView": {
                    controller: "realTimePriceCtrl",
                    templateUrl: function () {
                        return  "templates/query/realTimePrice.html";
                    }
                }
            },
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'starter.queryControllers',
                        files: ['js/queryControllers.js']
                    });
                }]
            }
        })

            /*我的订单*/
            .state('myOrder', {
                url: "/myOrder",
                views: {
                    "lazyLoadView": {
                        controller: "myOrderCtrl",
                        templateUrl: function () {
                            return  "templates/order/myOrder.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.orderControllers',
                            files: ['js/orderControllers.js']
                        });
                    }]
                }
            })
            /*我的订单详情*/
            .state('myOrderDetail', {
                url: "/myOrderDetail/:id/:Status",
                views: {
                    "lazyLoadView": {
                        controller: "myOrderDetailCtrl",
                        templateUrl: function () {
                            return  "templates/order/myOrderDetail.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.myOrderDetailControllers',
                            files: ['js/myOrderDetailControllers.js','js/orderControllers.js']
                        });
                    }]
                }
            })
            /*发货人管理*/
            .state('senderMng', {
                url: "/senderMng/:param",
                views: {
                    "lazyLoadView": {
                        controller: "senderMngCtrl",
                        templateUrl: function () {
                            return  "templates/order/senderMng.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.orderControllers',
                            files: ['js/orderControllers.js']
                        });
                    }]
                }
            })

            /*我要下单*/
            .state('placeOrder', {
                url: "/placeOrder",
                views: {
                    "lazyLoadView": {
                        controller: "placeOrderCtrl",
                        templateUrl: function () {
                            return  "templates/order/placeOrder.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.orderControllers',
                            files: ['js/orderControllers.js']
                        });
                    }]
                }
            })
            .state('goodsTrack', {
                url: "/goodsTrack/:id",
                views: {
                    "lazyLoadView": {
                        controller: "goodsTrackCtrl",
                        templateUrl: function () {
                            return  "templates/query/goodsTrack.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.queryControllers',
                            files: ['js/queryControllers.js']
                        });
                    }]
                }

            })

            /*货物 查询*/
            .state('goodSelect', {
            	url: "/goodSelect",
                views: {
                    "lazyLoadView": {
                        controller: "goodSelectCtrl",
                        templateUrl: function () {
                            return  "templates/query/goodSelect.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.goodSelectControllers',
                            files: ['js/goodSelectControllers.js']
                        });
                    }]
                }
            })

            /*我要下单*/
            .state('address', {
                url: "/address",
                views: {
                    "lazyLoadView": {
                        templateUrl: function(){
                            return "templates/query/address.html";
                        }
                    }
                }
            })

            /*服务范围列表查询界面*/
            .state('serviceQuery', {
                url: "/serviceQuery",
                views: {
                    "lazyLoadView": {
                        controller: "ServiceQueryCtrl",
                        templateUrl: function () {
                            return  "templates/service/serviceQuery.html";
                        }
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'starter.serviceQurey',
                            files: ['js/serviceQureyCtrl.js']
                        });
                    }]
                }
            })
             .state('baiDuCount', {
       	url: "/baiDuCount",
           views: {
               "lazyLoadView": {
                   controller: "baiDuCountCtrl",
                   templateUrl: function () {
                       return  "templates/baiDuCount.html";
                   }
               }
           },
           resolve: {
               loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                   return $ocLazyLoad.load({
                       name: 'starter.baiDuCountCtrl',
                       files: ['js/baiDuCountController.js']
                   });
               }]
           }
       })
                
    /*服务范围详情*/
          
   .state('serviceArea', {
       url: "/serviceArea/:id",
       views: {
           "lazyLoadView": {
              controller: "serviceAreaCtrl",
               templateUrl: function () {
          return  "templates/serviceArea.html";
      }
  }
},
resolve: {
  loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
      return $ocLazyLoad.load({
          name: 'starter.serviceAreaControllers',
          files: ['js/******.js','js/*******.js']
      });
  }]
}
})

	   /*百度统计*/
      

  
        $urlRouterProvider.otherwise('/home');

    });

