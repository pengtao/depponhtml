angular.module('starter.bindPhoneControllers', []) 
//绑定手机号页面
.controller('bindPhoneCtrl', function ($scope,$timeout,$http,$location,$rootScope) {
	$scope.isShow=true;//显示加载框
	$http.post('../login/getSessionInfo.jspa').success(function (json) {
		$scope.isShow=false;//隐藏加载框
    	var user =json.userRest;
    	$scope.telephone=user.telephone;
    	
    	if(user==null){
			$location.path("/login/myDeppon");
		}
	
	$scope.errorContent="";
    $scope.checkPhone= function(){
        if($scope.phone=="" |$scope.phone===undefined |!$scope.bindPhone.phone.$valid){
            $scope.errorContent="提示：请输入正确的新手机号！";
        }else if($scope.telephone==$scope.phone){
            $scope.errorContent="提示：新手机号与原手机号相同，请重设！";
            
        }else{
            $scope.errorContent="";
        }
    }
    $scope.checkCd= function(){
        if($scope.validateCode==""|$scope.validateCode===undefined| !$scope.bindPhone.validateCode.$valid){
            $scope.errorContent="提示：验证码不能为空！";
        }else{
            $scope.errorContent="";
        }
    }
    
    $scope.conform = function () {
     $scope.validateCode=$scope.bindPhone.validateCode.$viewValue;
     $scope.phone = $scope.bindPhone.phone.$viewValue;
     if($scope.phone==null) {
         $scope.errorContent="提示：信息不能为空!";
     }else{
         if ($scope.bindPhone.$valid) {
            var obj={"userName":user.userName,"phone":$scope.phone,"validateCode":$scope.validateCode};
            $scope.isShow=true;//显示加载框
            $http.post('../user/getPhoneReset.jspa',obj).success(function (json) {
            	$scope.isShow=false;//隐藏加载框
            	var detail = eval("("+json.detail+")");
                if (detail.code == '10000') {
                    //成功跳转到个人资料页面
                    $location.path("/personInformation");
                }else if(detail.code == '20015') {
                    $scope.errorContent = "提示：该手机号已被绑定，请核实";
                }else if(detail.code == '20001') {
                    $scope.errorContent = "提示：该手机号已被注册，请核实";
                }else if(detail.code == '20006'||detail.code == '20007') {
                    $scope.errorContent   = "提示：短信验证码错误，请核实";
                }else if(detail.code == '20008'){
                	$scope.errorContent = "提示：您的短信验证码已失效，请重新获取";
                }else{
                    $scope.errorContent = "提示：服务器繁忙！";
                }
                    $timeout(function () { 
                        $scope.errorContent = "";
                    }, 5000);
            });
    }
  } 
} 
     
    $scope.codeContent = '获取验证码';
    $scope.timeLength = 60;
    $scope.getcode = function (event) {
    	
    	//调用获取短信编码接口
        if ($scope.phone ===undefined | $scope.phone == null | $scope.phone == $scope.telephone) {
            $scope.errorContent="提示：请输入正确的新手机号！";
            event.target.disabled = false;
            $scope.codeContent = '获取验证码';
            $scope.timeLength = 60;
        } else {
    	if($scope.timeLength ==60){
     		//调用获取短信编码接口
            $scope.phone= $scope.bindPhone.phone.$viewValue;
            var obj={"phone":$scope.phone,"codeType":'1'};
            $http.post('../login/obtainMessageCode.jspa',obj).success(function(json) {
      			  });
     	}
    	
        event.target.disabled = true;
        $timeout(function () {
            $scope.timeLength--;
            $scope.codeContent = $scope.timeLength + '秒后重新发送';
            //倒计时结束后
            if ($scope.timeLength == 0) {
                $timeout.cancel();
                $scope.timeLength = 60;
                event.target.disabled = false;
                $scope.codeContent = '重新获取';
                return;
            }
            $scope.getcode(event);
        }, 1000);
        }
    };

    
	}).error(function () {
        $location.path("/login/myDeppon");
    });

    $scope.check=function(){  check($scope,$http,$location,$rootScope);}
    $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
})
;
