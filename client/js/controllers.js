function check($scope,$http,$location,$rootScope){
    $rootScope.goBackNot=true; 
    $scope.isShow=true;//显示加载框
    $http.post('../login/getSessionInfo.jspa')
        .success(function (json) {
    	$scope.isShow=false;//隐藏加载框
        $scope.user =json.userRest;
        if($scope.user==null){
            $location.path("/login/myDeppon");
        }else{
            $location.path("/myDeppon");
        }
    })
        .error(function () {
            $location.path("/login/myDeppon");
        });
};

function check2($scope,$http,$location,$rootScope){
    $rootScope.goBackNot=true;
    $scope.isShow=true;//显示加载框
    $http.post('../login/getSessionInfo.jspa')
        .success(function (json) {
    	$scope.isShow=false;//隐藏加载框
        $scope.user = json.userRest;
        if ($scope.user == null) {
            $location.path("/login/placeOrder");
        } else {
            $location.path("/placeOrder");
        }
    })
        .error(function () {
            $location.path("/login/placeOrder");
        });
};
var business=0;// 功能列表 1：我要下单 2：价格时效查询 3：网点查询 4：快递收送范围
function citys($scope, $http,$location,$ionicModal,$ionicScrollDelegate,$rootScope){
    /////////////
    $scope.area = {};
    $scope.openIndex;
    $scope.citycode = "热门城市";
    $scope.pageType = 0;
    $scope.show = true;
    var hostcityList= new Array();
    var cityList = new Array();
    var cityLists = new Array();
    var citys = new Array();
    var provinceName='';
    var hotcitynum=0;
    var swithCitys="";
    $scope.switchMonth = function (layoutId, index, name) {
    	//回到顶端
    	$ionicScrollDelegate.scrollTop();
    	$scope.hotCity=[];
        $scope.areaCodes=layoutId;
        $rootScope.controlSystemBackButton=true;
        $scope.pageType = $scope.pageType + 1;
        for (var i = 0; i < $scope.provinces.length; i++) {
            if ($scope.provinces[i].province_code == layoutId) {
                $scope.hotCity = $scope.provinces[i].citys;
                cityList = $scope.provinces[i].citys;
                $scope.areaValues=name;
                $scope.areaCodes=layoutId;
                provinceName=name;
                swithCitys=layoutId;
                break;
            }
        }
        for (var i = 0; i < cityLists.length; i++) {

            if (cityLists[i].city_code == layoutId) {
                $scope.hotCity = cityLists[i].areas;
                if($scope.pageType==1){//根据热门城市获得省份
                	hotcitynum=1;
                    $scope.pageType=2;
                    var pcode=cityLists[i].cityp;
                    for (var j = 0; j < $scope.provinces.length; j++) {
                        if ($scope.provinces[j].province_code == pcode) {
                            $scope.areaValues=$scope.provinces[j].province_name;
                            swithCitys=pcode;
                            break;
                        }
                    }
                }
                if($scope.areaValues.indexOf("-")>0){
                	$scope.areaValues=$scope.areaValues.substr(0,$scope.areaValues.indexOf("-"));
                }
                $scope.areaValues=$scope.areaValues+"-"+name;
                if(swithCitys.indexOf("^")>0){
                	swithCitys=swithCitys.substr(0,swithCitys.indexOf("^"));
                }
                swithCitys=swithCitys+"^"+layoutId;
                break;
            }
        }
        $scope.citycode=$scope.areaValues;
        $scope.show = false;
        if($scope.pageType==3){
            $scope.areaValues=$scope.areaValues+"-"+name;
            swithCitys=swithCitys+"^"+layoutId;
            $scope.modal.hide();
            if(business==4){
            	$scope.area.name=$scope.areaValues;
                $scope.area.code=$scope.areaCodes;
                $scope.toggle();
            }
        }

        //1：我要下单 2：价格时效查询 3：网点查询 4：快递收送范围 5:收货人新增 6：发货人新增
        if(business==1){
            if($scope.openIndex==1){
                $scope.orderEntity.contactCity=$scope.areaValues;
            }else{
                $scope.orderEntity.receiverCustCity=$scope.areaValues;
            }
        }else if(business==2){
            if($scope.openIndex==1){
                $scope.sel.arrivedAreaName=$scope.areaValues;
                $scope.sel.arrivedAreaCode=$scope.areaCodes;
            }else{
                $scope.sel.leavedAreaName=$scope.areaValues;
                $scope.sel.leavedAreaCode=$scope.areaCodes;
            }
        }else if(business==3){
            $scope.area.sendcity=$scope.areaValues;
            $scope.area.sendcityCode=swithCitys;
        }else if(business==4){
            $scope.area.name=$scope.areaValues;
        }else if(business==5){
            $scope.consigneeInfo.addrProCity=$scope.areaValues;
        }else if(business==6){
            $scope.consignorInfo.addrProCity=$scope.areaValues;
        }
        
    };

    $rootScope.controlSystemBackButton=false;
    $ionicModal.fromTemplateUrl('templates/selectCity/select-modal.html', {
        scope: $scope,
        animation: 'slide-in-right'
    }).then(function (modal) {
        $scope.modal = modal;
    });
    
    $scope.openModal = function (openIndex) {
//        cityList = new Array();
//        cityLists = new Array();
        /*citys = new Array();*/
        $scope.pageType=0;
        $scope.citycode = "热门城市";
        $scope.show = true;
        $scope.openIndex=openIndex;
        $rootScope.controlSystemBackButton=true;
        if(sessionStorage.citysinfo==null){
        	$scope.isShow=true;//显示加载框
            $http.get('../searchProvinces/selectProvinces.jspa?province_code=&city_code=&page=1&pageSize=10000&keywords=').success(function (json) {
            	$scope.isShow=false;//隐藏加载框
            	var detail=eval("("+json.detail+")");
                if(detail.status_Code=='200'){//请求成功
                    var hostcitys=eval("("+json.hotCitys+")").city;
                    if(detail.response_count>0){//有数据
                        citys=detail.response_body;
                        hostcityList = hostcitys;
                        $scope.hotCity =hostcitys;
                        $scope.provinces = citys;
                      //截取省市区名称长度（省截取前2位，市，区截取前3位）
                        for(var i=0;i<citys.length;i++){
                          if(citys[i].province_name.length>2&&citys[i].province_name!='内蒙古自治区'&&citys[i].province_name!='黑龙江省'){
                            citys[i].province_name = citys[i].province_name.substring(0,2)
                            for(var j=0;j<citys[i].citys.length;j++){
                              citys[i].citys[j].city_name = citys[i].citys[j].city_name.substring(0,4)
                              for(var n=0;n<citys[i].citys[j].areas.length;n++){
                                citys[i].citys[j].areas[n].area_name= citys[i].citys[j].areas[n].area_name.substring(0,4)
                                }
                            }
                          }else{
                              citys[i].province_name = citys[i].province_name.substring(0,3)
                              for(var j=0;j<citys[i].citys.length;j++){
                              citys[i].citys[j].city_name = citys[i].citys[j].city_name.substring(0,4)
                              for(var n=0;n<citys[i].citys[j].areas.length;n++){
                        				citys[i].citys[j].areas[n].area_name = citys[i].citys[j].areas[n].area_name.substring(0,4)
                            		}
                        		}
                            }
                        }
                        //把所有的市级城市放入这里
                        for(var i=0;i<$scope.provinces.length;i++){
                            for(var j=0;j<$scope.provinces[i].citys.length;j++){
                            	cityLists.push($scope.provinces[i].citys[j])
                            }
                        }
                        $scope.modal.show();
                        sessionStorage.citysinfo= angular.toJson(citys);
                        sessionStorage.hotCity= angular.toJson(hostcitys);
                    }else{
                        $scope.nullArea=true;
                    }
                }else{
                    $scope.nullArea=true;
                }
            });
        }else{
            $scope.modal.show();
            $scope.hotCity = JSON.parse(sessionStorage.hotCity);
            $scope.provinces = JSON.parse(sessionStorage.citysinfo);
            hostcityList = JSON.parse(sessionStorage.hotCity);
            citys = JSON.parse(sessionStorage.citysinfo);
            //把所有的市级城市放入这里
            for(var i=0;i<$scope.provinces.length;i++){
                for(var j=0;j<$scope.provinces[i].citys.length;j++){
                	cityLists.push($scope.provinces[i].citys[j])
                }
            }
        }


    };
    $scope.closeModal = function () {
    	$rootScope.controlSystemBackButton=false;
        if ($scope.pageType == "0") {
            $scope.modal.hide();
        } else if ($scope.pageType == "1") {
            $scope.show = true;
            cityList = cityLists;
            $scope.pageType = $scope.pageType - 1;
            $scope.hotCity = hostcityList;
            $scope.provinces = citys;
            $scope.citycode = "热门城市";
        } else if ($scope.pageType == "2") {
            $scope.pageType = $scope.pageType - 1;
            if(hotcitynum==1){
//                $scope.modal.hide();
                $scope.hotCity = hostcityList;
                $scope.provinces = citys;
                $scope.citycode = '热门城市';
                $scope.pageType = 0;
                hotcitynum = 0;
                $scope.show = true;
            }else{
            	 $scope.hotCity = cityList;
                 $scope.citycode = provinceName;
            }
        } else {
            $scope.modal.hide();
        }
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
//        $scope.modalReceiver.remove();
        $scope.modal.remove();
    });
    $scope.$on('$locationChangeStart', function(e) {
        if($rootScope.controlSystemBackButton){
            $scope.modal.hide();
            $rootScope.controlSystemBackButton = false;
            if($rootScope.goBackNot){
                $location.path($location.path());
            }else{
                //拦截系统返回按钮
                e.preventDefault();

            }
        }
    });
}
function clearNoNum(obj){
	if(obj==null){
		obj="";
	}
  obj = obj.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
  //obj = obj.replace(/^\./g,"");  //验证第一个字符是数字而不是.
  obj = obj.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
  obj = obj.replace(".","$#$").replace(/\./g,"").replace("$#$",".").replace(/(\.\d{2})\d*$/,'\$1');
  return obj;
}
//监听版本的更新，看是否已经更新下载完成
applicationCache.onupdateready = function(){
    applicationCache.swapCache();
    location.reload();
}
angular.module('starter.controllers', [])
    .controller('homeCtrl', function ($scope,$location,$http,$rootScope) {
    	//访问设置判断
    	/*function isMobile() {
    	    var ua = navigator.userAgent;
    	    isAndroid = /Android/i.test(ua);
    	    isBlackBerry = /BlackBerry/i.test(ua);
    	    isWindowPhone = /IEMobile/i.test(ua);
    	    isIOS = /iPhone|iPad|iPod/i.test(ua);
    	    isMobile = isAndroid || isBlackBerry || isWindowPhone || isIOS;
    	    if (isAndroid)
    	        isMobile = 'android';
    	    if (isBlackBerry)
    	        isMobile = 'BlackBerry';
    	    if (isWindowPhone)
    	        isMobile = 'WindowPhone';
    	    if (isIOS)
    	        isMobile = 'IOS';
    	    return isMobile;
    	}
	     var isMobile=isMobile();
	     if(isMobile==false){
	    	 window.location.href="http://www.deppon.com";
	     }else{
	    	 window.location.href="http://m.deppon.com";
	     }*/
      	$scope.loginGo=function(){
      		$scope.isShow=true;//显示加载框
	        $http.post('../login/getSessionInfo.jspa')
                .success(function (json) {
                    $scope.isShow = false;//隐藏加载框
                    $scope.user = json.userRest;
                    if ($scope.user == null) {
                        $location.path("/login/personInformation");
                    } else {
                        $location.path("/personInformation");
                    }
                })
                .error(function () {
                    $location.path("/login/personInformation");
                });
        }
        $scope.check=function(){  check($scope,$http,$location,$rootScope);}
        $scope.check2=function(){  check2($scope,$http,$location,$rootScope);}
    })

/**
 * HMTL转义
 */
    .filter('rawHtml', ['$sce', function($sce){
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    }])
var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
function baseMEncode(str) {
	var out, i, len;
	var c1, c2, c3;
	len = str.length;
	i = 0;
	out = "";
	while (i < len) {
		c1 = str.charCodeAt(i++) & 0xff;
		if (i == len) {
			out += base64EncodeChars.charAt(c1 >> 2);
			out += base64EncodeChars.charAt((c1 & 0x3) << 4);
			out += "==";
			break
		}
		c2 = str.charCodeAt(i++);
		if (i == len) {
			out += base64EncodeChars.charAt(c1 >> 2);
			out += base64EncodeChars.charAt(((c1 & 0x3) << 4)
					| ((c2 & 0xF0) >> 4));
			out += base64EncodeChars.charAt((c2 & 0xF) << 2);
			out += "=";
			break
		}
		c3 = str.charCodeAt(i++);
		out += base64EncodeChars.charAt(c1 >> 2);
		out += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
		out += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
		out += base64EncodeChars.charAt(c3 & 0x3F)
	}
	return out
}

;
