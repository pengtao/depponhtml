angular.module('starter.serviceQurey', [])
 .controller('ServiceQueryCtrl',function($scope, $document, $ionicModal, $http, $location, $ionicScrollDelegate, $rootScope){
        var pageIndex = 0;
        var hasMore = false,initial=true;
        var Bbranches = [];
        $scope.queryBranches = function(name, cityCode,sendcity, pageidx) {
            name = name ? name : "";
            var cCodes = [],citys=[],
                province_code, city_code, area_code; !! cityCode ? cCodes = cityCode.split("^") : "";
            !! sendcity ? citys = sendcity.split("-") : "";
            if (cCodes.length == 3) {
                province_code = cCodes[0];
                city_code = cCodes[1];
                area_code = cCodes[2];
            } else if (cCodes.length == 2) {
                province_code = cCodes[0];
                city_code = cCodes[1];
            } else {
                province_code = cCodes[0];
            }
            if(citys.length>0){
                $scope.cityName=citys[citys.length-1];
            }
            if (pageidx === 0) {
                pageIndex = pageidx;
                Bbranches = []
            }
            //TODO
            //       	 var obj = {'province_code':province_code,'city_code':city_code,'area_code':area_code,"name": name, "longitude":121.33017783194, "latitude": 31.251269499009,"pageSize":10,"pageIndex":++pageIndex};
            $scope.centerLongitude = 121.33017783194;
            $scope.centerLatitude = 31.251269499009;
            var obj = {
                'province_code': province_code,
                'city_code': city_code,
                'area_code': area_code,
                "keyWords": name,
                "longitude": $scope.centerLongitude,
                "latitude": $scope.centerLatitude,
                "pageSize": 10,
                "pageIndex": pageIndex++
            };
            hasMore = true;
            $http.post('../branchInfo/branchList.jspa', obj).success(function(json) {
                var detail = json.detail;
                ;
                hasMore = true;
                if (detail && detail.length > 0) {
                    detail = transDetail(detail);
                    for (var x = 0; x < detail.length; x++) {
                        Bbranches.push(detail[x]);
                    }

                    if (detail.length == 10) {
                        $scope.loadMoreWord="上拉查看更多";
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    } else {
                        hasMore = false;
                        $scope.loadMoreWord="";
                    }

                }
                $scope.branches = Bbranches;

            });
        };
        $scope.hasMoreData = function() {
            return hasMore;
        }
        business = 3;
        citys($scope, $http, $location, $ionicModal, $ionicScrollDelegate, $rootScope);
        $scope.check = function() {
            check($scope, $http, $location, $rootScope);
        }
        $scope.check2 = function() {
            check2($scope, $http, $location, $rootScope);
        }
        $scope.inFocus = function(e) {
            var target = e.target;
            target.blur();
        };
        $scope.getTelephones = function(tels) {
            var arrs = []
            if ( !! tels) {
                if (tels.indexOf("/") > -1) {
                    arrs = tels.split('/');
                    var arrl = arrs.length;
                    for (var i = 1; i < arrl; i++) {
                        var l = arrs[i].length;
                        arrs[i] = arrs[i - 1].slice(0, -l) + "" + arrs[i];
                    }
                } else {
                    arrs.push(tels)
                }
                return arrs;
            }

        }

        function transDetail(detail) {
            for (var i = 0; i < detail.length; i++) {
                var business = detail[i].business
                if (business == "11111") {
                    detail[i].business = "可发货 可自提 可派送"
                } else if (business == "01111") {
                    detail[i].business = "可自提 可派送 "
                } else if (business == "10111") {
                    detail[i].business = "可发货 可派送"
                } else if (business == "11011") {
                    detail[i].business = "可发货 可自提"
                } else if (business == "11101") {
                    detail[i].business = "可发货 可自提 可派送 "
                } else if (business == "11110") {
                    detail[i].business = "可发货 可自提 可派送 "
                } else if (business == "00111") {
                    detail[i].business = "可派送 "
                } else if (business == "01011") {
                    detail[i].business = "可自提 "
                } else if (business == "01101") {
                    detail[i].business = "可自提 可派送 "
                } else if (business == "01110") {
                    detail[i].business = "可自提 可派送 "
                } else if (business == "10011") {
                    detail[i].business = "可发货 "
                } else if (business == "10101") {
                    detail[i].business = "可发货 可派送 "
                } else if (business == "10110") {
                    detail[i].business = "可发货 可派送"
                } else if (business == "11001") {
                    detail[i].business = "可发货 可自提"
                } else if (business == "11010") {
                    detail[i].business = "可发货 可自提 "
                } else if (business == "11100") {
                    detail[i].business = "可发货 可自提 可派送"
                } else if (business == "00011") {
                    detail[i].business = ""
                } else if (business == "00101") {
                    detail[i].business = "可派送 "
                } else if (business == "00110") {
                    detail[i].business = " 可派送 "
                } else if (business == "01001") {
                    detail[i].business = "可自提 "
                } else if (business == "01010") {
                    detail[i].business = "可自提"
                } else if (business == "01100") {
                    detail[i].business = "可自提 可派送"
                } else if (business == "00001") {
                    detail[i].business = ""
                } else if (business == "00010") {
                    detail[i].business = ""
                } else if (business == "00100") {
                    detail[i].business = "可派送"
                } else if (business == "01000") {
                    detail[i].business = "可自提"
                } else if (business == "10001") {
                    detail[i].business = "可发货 "
                } else if (business == "10010") {
                    detail[i].business = "可发货 "
                } else if (business == "10100") {
                    detail[i].business = "可发货 可派送"
                } else if (business == "11000") {
                    detail[i].business = "可发货 可自提"
                } else if (business == "10000") {
                    detail[i].business = "可发货"
                } else if (business == "00000") {
                    detail[i].business = ""
                }
                detail[i].oldtel = detail[i].telephone;
                detail[i].telephone = $scope.getTelephones(detail[i].telephone);
            }
            return detail;

        };
        $scope.branchDetail=function(value){
           $location.path("/serviceArea/value");
        };
    })