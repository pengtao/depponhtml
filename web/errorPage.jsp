<%@ page language="java" contentType="text/html;charset=gb2312"
    pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ERROR.JSP</title>
<script type="text/javascript">
	function operate(){
		var excep = document.getElementById("exception_ta").style.display;
		if(excep == "none"){
			document.getElementById("exception_ta").style.display = "block";
		}else{
			document.getElementById("exception_ta").style.display = "none";
		}
	}
</script>
</head>
<body>

对不起，系统繁忙，请稍后重试。<a href="toWorkItemsList">返回上页</a>
<br>
<h3 onclick="operate()">详细信息</h3>
<table width="" border="0" id="exception_ta" style="display: none">
<tr valign="top">
<td width=""><b>Error:</b></td>
<td>${pageContext.exception}</td>
</tr>
<tr valign="top">
<td><b>URI:</b></td>
<td>${pageContext.errorData.requestURI}</td>
</tr>
<tr valign="top">
<td><b>Status code:</b></td>
<td>${pageContext.errorData.statusCode}</td>
</tr>
<tr valign="top">
<td><b>Stack trace:</b></td>
<td>
<% StackTraceElement[] eme = exception.getStackTrace();
 	for(StackTraceElement em:eme){%>
	<%=em %>
<%}%>
</td>
</tr>
</table>
</body>
</html>